#ifndef __EXPR_H__
#define __EXPR_H__
#include <iostream>

using namespace std;
class ExprNode;
class Expr;
class ExprNode
{
    friend ostream& operator<<(ostream&, const ExprNode&);
    friend ostream& operator<<(ostream&, const Expr&);
    friend Expr;
    int use;
protected:
    ExprNode():use(1){}
    virtual void print(ostream&) const = 0;
    virtual ~ExprNode(){}
};
ostream& operator<<(ostream& o, const ExprNode& t){
    t.print(o);
    return o;
}
class InitNode :public ExprNode
{
    friend class Expr;
    int n;

    InitNode(int k) :n(k){}
    void print(ostream& o) const {o << n;}
};

class UnaryNode :public ExprNode
{
    friend class Expr;
    string op;
    ExprNode* opnd;
    UnaryNode(const string& a, ExprNode* b) :op(a),opnd(b){}
    void print(ostream& o) const {o <<"(" << op << *opnd << ")";}
};
class BinaryNode :public ExprNode
{
    friend class Expr;
    string op;
    ExprNode* left;
    ExprNode* right;
    BinaryNode(const string& a, ExprNode* b,ExprNode* c) :op(a),left(b),right(c){}
    void print(ostream& o) const {o <<"(" << *left << op << *right << ")";}
};

class Expr
{
    
private:
    friend ostream& operator<<(ostream&, const Expr&);
    ExprNode* p;
public:
    Expr(int);
    Expr(const string&, Expr);
    Expr(const string&, Expr, Expr);
    Expr(const Expr& t){ p = t.p; ++p->use; };
    Expr& operator=(const Expr&);
    ~Expr() {
        if (--p->use == 0)
        {
            delete p;
        }
    }
};

Expr::Expr(int n)
{
    p = new InitNode(n);
}
Expr::Expr(const string& op, Expr t)
{
    p = new UnaryNode(op,t.p);
}
Expr::Expr(const string& op, Expr left, Expr right)
{
    p = new BinaryNode(op,left.p,right.p);
}
Expr& Expr::operator=(const Expr& rhs){
    rhs.p->use++;
    if (--p->use == 0)
    {
        delete p;
    }
    p = rhs.p;
    return *this;
}

ostream& operator<<(ostream& o, const Expr& t){
    t.p->print(o);
    return o;
}






#endif




