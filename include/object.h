#ifndef __OBJECT_H__
#define __OBJECT_H__
#include <iostream>
#include <mutex>
#include <atomic>
#include <iostream>
#include <typeinfo>

#define RB_DLL __attribute__((visibility("default")))


class RefCount {
  std::atomic<int> count;

 public:
  RefCount() noexcept : count(0) {}
  int increment() { return ++count; }  // Increment and return new value
  int decrement() { return --count; }  // Decrement and return new value
  bool is_zero() const { return count == 0; }

  int num() const { return count; }
};

template <typename T>
class ObjectPtr;

class ObjectRef;

/** The abstract base classes for a node in the Halide IR. */
struct Object {
  /*!
   * \brief Object deleter
   * \param self pointer to the Object.
   */
  typedef void (*FDeleter)(Object *self);

  Object() = default;
  Object(uint32_t type_index) : type_index_(type_index) {}

  virtual ~Object() {}

  /** These classes are all managed with intrusive reference
   * counting, so we also track a reference count. It's mutable
   * so that we can do reference counting even through const
   * references to IR nodes.
   */
  mutable RefCount ref_count;

  // reference counter related operations
  /*! \brief developer function, increases reference counter. */
  void IncRef();
  /*!
   * \brief developer function, decrease reference counter.
   * \note The deleter will be called when ref_counter_ becomes zero.
   */
  void DecRef();

  template <typename T>
  inline bool IsInstance() const {
    return DerivedFrom(this->type_index_, T::TypeIndex());
  }

  char const *GetTypeKey() const;

  static uint32_t TypeIndex() { return 0; }

  static char const *GetTypeKey(uint32_t);

  static bool FindTypeIndex(char const *, uint32_t *);

  uint32_t type_index_;
  static constexpr const char *_type_key = "Object";

 protected:
  static bool DerivedFrom(uint32_t child_tindex, uint32_t parent_tindex);

  static uint32_t GetOrAllocTypeIndex(const std::string &type_key,
                                      uint32_t parent_tidx);

  FDeleter deleter_ = nullptr;

  template <typename U, typename... Args>
  friend ObjectPtr<U> make_object(Args &&...args);
};

/*!
 * \brief A custom smart pointer for Object.
 * \tparam T the content data type.
 * \sa make_object
 */
template <typename T>
class ObjectPtr {
 public:
  /*! \brief default constructor */
  ObjectPtr() {}
  /*! \brief default constructor */
  ObjectPtr(std::nullptr_t) {}  // NOLINT(*)
  /*!
   * \brief copy constructor
   * \param other The value to be moved
   */
  ObjectPtr(const ObjectPtr<T> &other)  // NOLINT(*)
      : ObjectPtr(other.data_) {}
  /*!
   * \brief copy constructor
   * \param other The value to be moved
   */
  template <typename U>
  ObjectPtr(const ObjectPtr<U> &other)  // NOLINT(*)
      : ObjectPtr(other.data_) {
    static_assert(std::is_base_of<T, U>::value,
                  "can only assign of child class ObjectPtr to parent");
  }
  /*!
   * \brief move constructor
   * \param other The value to be moved
   */
  ObjectPtr(ObjectPtr<T> &&other)  // NOLINT(*)
      : data_(other.data_) {
    other.data_ = nullptr;
  }
  /*!
   * \brief move constructor
   * \param other The value to be moved
   */
  template <typename Y>
  ObjectPtr(ObjectPtr<Y> &&other)  // NOLINT(*)
      : data_(other.data_) {
    static_assert(std::is_base_of<T, Y>::value,
                  "can only assign of child class ObjectPtr to parent");
    other.data_ = nullptr;
  }
  /*! \brief destructor */
  ~ObjectPtr() { this->reset(); }
  /*!
   * \brief Swap this array with another Object
   * \param other The other Object
   */
  void swap(ObjectPtr<T> &other) {  // NOLINT(*)
    std::swap(data_, other.data_);
  }
  /*!
   * \return Get the content of the pointer
   */
  T *get() const { return static_cast<T *>(data_); }
  /*!
   * \return The pointer
   */
  T *operator->() const { return get(); }
  /*!
   * \return The reference
   */
  T &operator*() const {  // NOLINT(*)
    return *get();
  }
  /*!
   * \brief copy assignmemt
   * \param other The value to be assigned.
   * \return reference to self.
   */
  ObjectPtr<T> &operator=(const ObjectPtr<T> &other) {  // NOLINT(*)
    // takes in plane operator to enable copy elison.
    // copy-and-swap idiom
    ObjectPtr(other).swap(*this);  // NOLINT(*)
    return *this;
  }
  /*!
   * \brief move assignmemt
   * \param other The value to be assigned.
   * \return reference to self.
   */
  ObjectPtr<T> &operator=(ObjectPtr<T> &&other) {  // NOLINT(*)
    // copy-and-swap idiom
    ObjectPtr(std::move(other)).swap(*this);  // NOLINT(*)
    return *this;
  }
  /*! \brief reset the content of ptr to be nullptr */
  void reset() {
    if (data_ != nullptr) {
      data_->DecRef();
      data_ = nullptr;
    }
  }
  /*! \return The use count of the ptr, for debug purposes */
  int use_count() const {
    return data_ != nullptr ? data_->ref_count.num() : 0;
  }
  /*! \return whether the reference is unique */
  bool unique() const {
    return data_ != nullptr && data_->ref_count.num() == 1;
  }
  /*! \return Whether two ObjectPtr do not equal each other */
  bool operator==(const ObjectPtr<T> &other) const {
    return data_ == other.data_;
  }
  /*! \return Whether two ObjectPtr equals each other */
  bool operator!=(const ObjectPtr<T> &other) const {
    return data_ != other.data_;
  }
  /*! \return Whether the pointer is nullptr */
  bool operator==(std::nullptr_t null) const { return data_ == nullptr; }
  /*! \return Whether the pointer is not nullptr */
  bool operator!=(std::nullptr_t null) const { return data_ != nullptr; }

 private:
  /*! \brief internal pointer field */
  Object *data_{nullptr};
  /*!
   * \brief constructor from Object
   * \param data The data pointer
   */
  explicit ObjectPtr(Object *data) : data_(data) {
    if (data != nullptr) {
      data_->IncRef();
    }
  }

  template <typename U, typename... Args>
  friend ObjectPtr<U> make_object(Args &&...args);

  template <typename RelayRefType, typename ObjType>
  friend RelayRefType GetRef(const ObjType *ptr);

  template <typename BaseType, typename ObjType>
  friend ObjectPtr<BaseType> GetObjectPtr(ObjType *ptr);

  template <typename U>
  friend class ObjectPtr;

  friend void FFIClearAfterMove(ObjectRef *ref);
};

template <typename T>
inline void default_obj_deleter(T *obj) {
  // std::cout << "need to delete" <<std::endl;
  delete obj;
}

template <typename T, typename... Args>
inline ObjectPtr<T> make_object(Args &&...args) {
  T *res = new T(std::forward<Args>(args)...);
  res->type_index_ = T::TypeIndex();
  res->deleter_ = &default_obj_deleter;
  return ObjectPtr<T>(res);
}

/** IR nodes are passed around opaque handles to them. This is a
   base class for those handles. It manages the reference count,
   and dispatches visitors. */
// struct RB_DLL ObjectRef {
struct RB_DLL ObjectRef {
  ObjectRef() = default;
  ObjectRef(ObjectPtr<Object> p) : ptr(p) {}

  /** Downcast this ir node to its actual type (e.g. Add, or
   * Select). This returns nullptr if the node is not of the requested
   * type. Example usage:
   *
   * if (const Add *add = node->as<Add>()) {
   *   // This is an add node
   * }
   */
  template <typename T>
  inline const T *as() const {
    if (is<T>()) {
      return static_cast<T const *>(ptr.get());
    } else {
      return nullptr;
    }
  }

  template <typename T>
  inline T *as() {
    if (is<T>()) {
      return static_cast<T *>(ptr.get());
    } else {
      return nullptr;
    }
  }

  template <typename T>
  inline bool is() const {
    return ptr != nullptr && ptr->IsInstance<T>();
  }

  /*!
   * \brief Comparator
   * \param other Another object ref.
   * \return the compare result.
   */
  bool same_as(const ObjectRef &other) const { return ptr == other.ptr; }
  /*!
   * \brief Comparator
   * \param other Another object ref.
   * \return the compare result.
   */
  bool operator==(const ObjectRef &other) const { return ptr == other.ptr; }
  /*!
   * \brief Comparator
   * \param other Another object ref.
   * \return the compare result.
   */
  bool operator!=(const ObjectRef &other) const { return ptr != other.ptr; }
  /*!
   * \brief Comparator
   * \param other Another object ref by address.
   * \return the compare result.
   */
  bool operator<(const ObjectRef &other) const {
    return ptr.get() < other.ptr.get();
  }
  /*!
   * \return whether the object is defined(not null).
   */
  bool defined() const { return ptr != nullptr; }
  /*! \return the internal object pointer */
  Object *get() const { return ptr.get(); }
  /*! \return the internal object pointer */
  Object *operator->() const { return get(); }

  /*! \return whether the reference is unique */
  bool unique() const { return ptr.unique(); }
  /*! \return The use count of the ptr, for debug purposes */
  int use_count() const { return ptr.use_count(); }

  explicit operator bool() const { return defined(); }

  std::string to_string() const;

  void dump() const;

  /*! \brief type indicate the container type. */
  using ContainerType = Object;
  static constexpr bool _type_is_nullable = true;

  ObjectPtr<Object> ptr;

  friend void FFIClearAfterMove(ObjectRef *ref);

  template <typename SubRef, typename BaseRef>
  friend SubRef Downcast(BaseRef ref);
};

struct ObjectHash {
  size_t operator()(ObjectRef const &a) const {
    return std::hash<Object const *>()(a.get());
  }
};

/*!
 * \brief Clear the object ref data field without DecRef
 *        after we successfully moved the field.
 * \param ref The reference data.
 */
inline void FFIClearAfterMove(ObjectRef *ref) {
  ref->ptr.data_ = nullptr;
}

template <typename BaseType, typename ObjType>
inline ObjectPtr<BaseType> GetObjectPtr(ObjType *ptr) {
  static_assert(std::is_base_of<BaseType, ObjType>::value,
                "Can only cast to the ref of same container type");
  return ObjectPtr<BaseType>(static_cast<Object *>(ptr));
}

template <typename RefType, typename ObjType>
inline RefType GetRef(const ObjType *ptr) {
  static_assert(
      std::is_base_of<typename RefType::ContainerType, ObjType>::value,
      "Can only cast to the ref of same container type");
  return RefType(ObjectPtr<Object>(
      const_cast<Object *>(static_cast<const Object *>(ptr))));
}

template <typename SubRef, typename BaseRef>
inline SubRef Downcast(BaseRef ref) {
  using SubObj = typename SubRef::ContainerType;
  if (ref.defined()) {
    CHECK(ref->template IsInstance<SubObj>())
        << "Downcast from " << ref->GetTypeKey() << " to "
        << std::string(SubObj::_type_key) << " failed.";
  } else {
    CHECK(SubRef::_type_is_nullable)
        << "Downcast from nullptr to not nullable reference of "
        << std::string(SubObj::_type_key);
  }
  return SubRef(std::move(ref.ptr));
}

template <typename T>
static T DowncastNoCheck(ObjectRef ref) {
  return T(std::move(ref.ptr));
}

// using Downcast;
// using DowncastNoCheck;
// using GetRef;
using ::ObjectPtr;
using ::ObjectRef;

#define RB_DECLACE_OBJECT_INFO(TypeName, ParentType)                   \
  static uint32_t TypeIndex() {                                        \
    static uint32_t tidx = ::rb::runtime::Object::GetOrAllocTypeIndex( \
        TypeName::_type_key, ParentType::TypeIndex());                 \
    return tidx;                                                       \
  }
/*
 * \brief Define the default copy/move constructor and assign opeator
 * \param TypeName The class typename.
 */
#define RB_DEFINE_DEFAULT_COPY_MOVE_AND_ASSIGN(TypeName) \
  TypeName(const TypeName &other) = default;             \
  TypeName(TypeName &&other) = default;                  \
  TypeName &operator=(const TypeName &other) = default;  \
  TypeName &operator=(TypeName &&other) = default;

#define RB_DEFINE_OBJECT_REF_METHODS(TypeName, ParentType, ObjectName) \
  TypeName() {}                                                        \
  RB_DEFINE_DEFAULT_COPY_MOVE_AND_ASSIGN(TypeName)                     \
  TypeName(::rb::runtime::ObjectPtr<::rb::runtime::Object> n)          \
      : ParentType(n) {}                                               \
  const ObjectName *operator->() const {                               \
    CHECK_NONULL(get());                                               \
    return static_cast<const ObjectName *>(get());                     \
  }                                                                    \
  using ContainerType = ObjectName;

#define RB_DEFINE_NOTNULLABLE_OBJECT_REF_METHODS(TypeName, ParentType, \
                                                 ObjectName)           \
  TypeName(::rb::runtime::ObjectPtr<::rb::runtime::Object> n)          \
      : ParentType(n) {                                                \
    CHECK(n != nullptr);                                               \
  }                                                                    \
  RB_DEFINE_DEFAULT_COPY_MOVE_AND_ASSIGN(TypeName)                     \
  const ObjectName *operator->() const {                               \
    CHECK_NONULL(get());                                               \
    return static_cast<const ObjectName *>(get());                     \
  }                                                                    \
  static constexpr bool _type_is_nullable = true;                      \
  using ContainerType = ObjectName;

#define RB_STR_CONCAT_(__x, __y) __x##__y
#define RB_STR_CONCAT(__x, __y) RB_STR_CONCAT_(__x, __y)




#endif