#ifndef __TEST_H__
#define __TEST_H__
#include <stdio.h>

using namespace std;

// An abstract class with constructor 

/**
 * @brief 函数指针
 */
typedef void (*Fun)();

/**
 * @brief 基类
 */
class Base
{
    // virtual static void fun11()  { } //error
    // static void fun12() const { } //error
     private: 
        
        // friend int main(); 
    public:
        Base(){};
        inline virtual void who()
        {
            cout << "I am Base\n";
        }
        virtual void fun_p() { cout << "Base Fun"; } 
        virtual void fun ( int x = 10 ) 
        { 
            cout << "Base::fun(), x = " << x << endl; 
        } 
        virtual void fun1()
        {
            cout << "Base::fun1()" << endl;
        }
        virtual void fun1(int x)
        {
            cout << "Base::fun1(int x)" << endl;
        }
        virtual void fun2()
        {
            cout << "Base::fun2()" << endl;
        }
        virtual void fun3(){cout << "Base::fun3()" << endl;}
        virtual ~Base(){};
};

/**
 * @brief 派生类
 */
class Derived: public Base
{
    private:
        void fun_p() { cout << "Derived Fun"; } 

    public:
        inline virtual void who()
        {
            cout << "I am Derived\n";
        }
        Derived(){};
        virtual void fun ( int x=20 ) 
        { 
            cout << "Derived::fun(), x = " << x << endl; 
        } 
        void fun1()
        {
            cout << "Derived::fun1()" << endl;
        }
        void fun1(int x)
        {
            cout << "Derived::fun1(x)" << endl;
        }
        void fun2()
        {
            cout << "DerivedClass::fun2()" << endl;
        }
        // void fun3()
        // {
        //     cout << "DerivedClass::fun3()" << endl;
        // }
        ~Derived(){};
};


#endif