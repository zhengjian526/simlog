#pragma once
#include "object.h"
#include <string.h>

/*!
 * \brief The data type the tensor can hold.
 *
 *  Examples
 *   - float: type_code = 2, bits = 32, lanes=1
 *   - float4(vectorized 4 float): type_code = 2, bits = 32, lanes=4
 *   - int8: type_code = 0, bits = 8, lanes=1
 */
typedef struct {
  /*!
   * \brief Type code of base types.
   * We keep it uint8_t instead of RBDataTypeCode for minimal memory
   * footprint, but the value should be one of RBDataTypeCode enum values.
   * */
  uint8_t code;
  /*!
   * \brief Number of bits, common choices are 8, 16, 32.
   */
  uint8_t bits;
  /*! \brief Number of lanes in the type, used for vector types. */
  uint16_t lanes;
} RBDataType;
/*! \brief Extension type code in RB */
typedef enum {
  kRBInt = 0U,
  kRBUInt = 1U,
  kRBFloat = 2U,
  kRBBfloat = 4U,
  kRBStr = 10U,
  kRBObject = 11U,
  kRBTensor = 12U,
  kRBPackedFunc = 13U,
  kRBOpaqueHandle = 99U,
  kRBNullptr = 100U
} RBTypeCode;

class DataTypeObj : public Object {
 public:
  RBDataType dtype;

  static constexpr const char* _type_key = "DataType";
//   RB_DECLACE_OBJECT_INFO(DataTypeObj, Object)
};

/*!
 * \brief Runtime primitive data type.
 *
 *  This class is a thin wrapper of RBDataType.
 *  We also make use of DataType in compiler to store quick hint
 */
class DataType : public ObjectRef {
 public:
  /*! \brief Type code for the DataType. */
  enum TypeCode { kInt = kRBInt, kUInt = kRBUInt, kFloat = kRBFloat };
  /*!
   * \brief Constructor
   * \param dtype The RBDataType
   */
  DataType(RBDataType dtype) {
    auto n = make_object<DataTypeObj>();
    n->dtype = dtype;
    ptr = n;
  }
  /*!
   * \brief Constructor
   * \param code The type code.
   * \param bits The number of bits in the type.
   * \param lanes The number of lanes.
   */
  DataType(int code, int bits, int lanes) {
    auto n = make_object<DataTypeObj>();
    n->dtype.code = static_cast<uint8_t>(code);
    n->dtype.bits = static_cast<uint8_t>(bits);
    n->dtype.lanes = static_cast<uint16_t>(lanes);
    ptr = n;
  }

  explicit DataType(std::string const& name);

  /*! \return The type code. */
  int code() const {
    return static_cast<int>(this->operator RBDataType().code);
  }
  /*! \return number of bits in the data. */
  int bits() const {
    return static_cast<int>(this->operator RBDataType().bits);
  }
  /*! \return number of bytes to store each scalar. */
  int bytes() const { return (bits() + 7) / 8; }
  /*! \return number of lanes in the data. */
  int lanes() const {
    return static_cast<int>(this->operator RBDataType().lanes);
  }
  /*! \return whether type is a scalar type. */
  bool is_scalar() const { return lanes() == 1; }
  /*! \return whether type is a scalar type. */
  bool is_bool() const { return code() == DataType::kUInt && bits() == 1; }
  /*! \return whether type is a float type. */
  bool is_float() const { return code() == DataType::kFloat; }
  /*! \return whether type is a float16 type. */
  bool is_float16() const { return is_float() && bits() == 16; }
  /*! \return whether type is an int type. */
  bool is_int() const { return code() == DataType::kInt; }
  /*! \return whether type is an uint type. */
  bool is_uint() const { return code() == DataType::kUInt; }
  /*! \return whether type is a vector type. */
  bool is_vector() const { return lanes() > 1; }
  /*! \return whether type is a bool vector type. */
  bool is_vector_bool() const { return is_vector() && bits() == 1; }
  /*!
   * \brief Create a new data type by change lanes to a specified value.
   * \param lanes The target number of lanes.
   * \return the result type.
   */
  DataType with_lanes(int lanes) const {
    return DataType(code(), bits(), lanes);
  }
  /*!
   * \brief Create a new data type by change bits to a specified value.
   * \param bits The target number of bits.
   * \return the result type.
   */
  DataType with_bits(int bits) const { return DataType(code(), bits, lanes()); }
  /*!
   * \brief Get the scalar version of the type.
   * \return the result type.
   */
  DataType element_of() const { return with_lanes(1); }
  /*!
   * \brief Equal comparator.
   * \param other The data type to compre against.
   * \return The comparison resilt.
   */
  bool operator==(const DataType& other) const {
    return code() == other.code() && bits() == other.bits() &&
           lanes() == other.lanes();
  }
  /*!
   * \brief NotEqual comparator.
   * \param other The data type to compre against.
   * \return The comparison resilt.
   */
  bool operator!=(const DataType& other) const { return !operator==(other); }
  /*!
   * \brief Converter to RBDataType
   * \return the result.
   */
  operator RBDataType() const {
    // if(this->defined()) return (*this)->dtype;
  }

  std::string ToString() const;

  /*!
   * \brief Construct an int type.
   * \param bits The number of bits in the type.
   * \param lanes The number of lanes.
   * \return The constructed data type.
   */
  static DataType Int(int bits, int lanes = 1) {
    return DataType(kRBInt, bits, lanes);
  }
  /*!
   * \brief Construct an uint type.
   * \param bits The number of bits in the type.
   * \param lanes The number of lanes
   * \return The constructed data type.
   */
  static DataType UInt(int bits, int lanes = 1) {
    return DataType(kRBUInt, bits, lanes);
  }
  /*!
   * \brief Construct an uint type.
   * \param bits The number of bits in the type.
   * \param lanes The number of lanes
   * \return The constructed data type.
   */
  static DataType Float(int bits, int lanes = 1) {
    return DataType(kRBFloat, bits, lanes);
  }
  /*!
   * \brief Construct a bool type.
   * \param lanes The number of lanes
   * \return The constructed data type.
   */
  static DataType Bool(int lanes = 1) { return DataType::UInt(1, lanes); }

//   RB_DEFINE_OBJECT_REF_METHODS(DataType, ObjectRef, DataTypeObj)
};