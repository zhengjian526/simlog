// #include "object.h"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include<iostream>
#include"log.h"
#include<stdio.h>
using namespace std;

#define ICHECK_INDENT "  "

class LogCheckError {
 public:
  LogCheckError() : str(nullptr) {}
  explicit LogCheckError(const std::string& str_) : str(new std::string(str_)) {cout << str_ <<endl;}
  LogCheckError(const LogCheckError& other) = delete;
  LogCheckError(LogCheckError&& other) : str(other.str) {
    other.str = nullptr;
  }
  ~LogCheckError() { if (str != nullptr) delete str; }
  operator bool() const { return str != nullptr; }
  LogCheckError& operator=(const LogCheckError& other) = delete;
  LogCheckError& operator=(LogCheckError&& other) = delete;
  std::string* str;
};
//-----------------logging.h--------------------
// #define DEFINE_CHECK_FUNC(name, op)                               \
//   template <typename X, typename Y>                               \
//   inline LogCheckError LogCheck##name(const X& x, const Y& y) {   \
//     if (x op y) return LogCheckError();                           \
//     std::ostringstream os;                                        \
//     os << " (" << x << " vs. " << y << ") ";  /* CHECK_XX(x, y) requires x and y can be serialized to string. Use CHECK(x OP y) otherwise. NOLINT(*) */ \
//     return LogCheckError(os.str());                               \
//   }                                                               \
//   inline LogCheckError LogCheck##name(int x, int y) {             \
//     return LogCheck##name<int, int>(x, y);                        \
//   }
#define DEFINE_CHECK_FUNC(name, op)                               \
  template <typename X, typename Y>                               \
  inline LogCheckError LogCheck##name(const X& x, const Y& y) {   \
    if (x op y) return LogCheckError();                           \
    return LogCheckError("Error.");                               \
  }                                                               \
  inline LogCheckError LogCheck##name(int x, int y) {             \
    return LogCheck##name<int, int>(x, y);                        \
  }

  DEFINE_CHECK_FUNC(_LT, <)
  DEFINE_CHECK_FUNC(_GT, >)
  DEFINE_CHECK_FUNC(_LE, <=)
  DEFINE_CHECK_FUNC(_GE, >=)
  DEFINE_CHECK_FUNC(_EQ, ==)
  DEFINE_CHECK_FUNC(_NE, !=)
//-----------------------------------------------
  class DummyOStream {
  public:
    template <typename T>
    DummyOStream& operator<<(T _) { cout << _ << endl; return *this; }
    inline std::string str() { return ""; }
  };
  class LogMessage {
  public:
    LogMessage(const char* file, int line) : log_stream_() {}
    DummyOStream& stream() { return log_stream_; }

  protected:
    DummyOStream log_stream_;

  private:
    LogMessage(const LogMessage&);
    void operator=(const LogMessage&);
  };

  class LogMessageFatal : public LogMessage {
  public:
    LogMessageFatal(const char* file, int line) : LogMessage(file, line) {cout << "file:"<<file << "line::"<< line <<endl;}
    ~LogMessageFatal() {
      // abort();
    }
  private:
    LogMessageFatal(const LogMessageFatal&);
    void operator=(const LogMessageFatal&);
  };
constexpr const char* kTVM_INTERNAL_ERROR_MESSAGE = "aaa=======";
#define ICHECK_BINARY_OP(name, op, x, y)                           \
  if (LogCheckError _check_err = LogCheck##name(x, y))             \
  LogMessageFatal(__FILE__, __LINE__).stream()                     \
      << kTVM_INTERNAL_ERROR_MESSAGE                               \
      << ICHECK_INDENT << "Check failed: " << #x " " #op " " #y << *(_check_err.str) << ": "


#define ICHECK(x)                                    \
  if (!(x))                                          \
  LogMessageFatal(__FILE__, __LINE__).stream() \
      << kTVM_INTERNAL_ERROR_MESSAGE << ICHECK_INDENT << "Check failed: " #x << " == false: "

#define ICHECK_LT(x, y) ICHECK_BINARY_OP(_LT, <, x, y)
#define ICHECK_GT(x, y) ICHECK_BINARY_OP(_GT, >, x, y)
#define ICHECK_LE(x, y) ICHECK_BINARY_OP(_LE, <=, x, y)
#define ICHECK_GE(x, y) ICHECK_BINARY_OP(_GE, >=, x, y)
#define ICHECK_EQ(x, y) ICHECK_BINARY_OP(_EQ, ==, x, y)
#define ICHECK_NE(x, y) ICHECK_BINARY_OP(_NE, !=, x, y)
#define ICHECK_NOTNULL(x)                                                    \
  ((x) == nullptr ? LogMessageFatal(__FILE__, __LINE__).stream()       \
                        << kTVM_INTERNAL_ERROR_MESSAGE << ICHECK_INDENT \
                        << "Check not null: " #x << ' ',                     \
   (x) : (x))  // NOLINT(*)



int main(){
      const volatile int local = 10; 
    int *ptr = (int*) &local; 

    printf("Initial value of local : %d \n", local); 

    *ptr = 100; 

    printf("Modified value of local: %d \n", local); 

    return 0; 

    // const int local = 10; 
    // int *ptr = (int*) &local; 

    // printf("Initial value of local : %d \n", local); 

    // *ptr = 100; 

    // printf("Modified value of local: %d \n", local); 

    // return 0; 
  cout << "start==" <<endl;
//   ICHECK_LT(1, 0) << "One and only one dim can be inferred";
//   ICHECK_NOTNULL(nullptr);
//   LogMessageFatal b(__FILE__, __LINE__);
//   b.stream() << ("aaaaaddada") ;
// #define AAA 0
  #ifndef AAA
  UINT8 szLogContent[1024] = {0};
  #endif
  // char str[10]={0};
  // snprintf(str, sizeof(str), "0123456789012345678");
  // printf("str=%s\n", str);
  // cout  << str <<endl;
	 // 获取配置文件中各个配置项的值
	GetConfigValue();
	
	// 先打印版本相关信息
	snprintf((char*)szLogContent, sizeof(szLogContent)-1, "Version [1.0], Build time[%s %s].", __DATE__, __TIME__);
  WRITELOGFILE(LOG_INFO, szLogContent);

	// 打印第一条日志
	snprintf(szLogContent, sizeof(szLogContent)-1, "The Fatal log info!");
    WRITELOGFILE(LOG_FATAL, szLogContent);

    // 打印第二条日志
	snprintf(szLogContent, sizeof(szLogContent)-1, "The Error log info!");
	WRITELOGFILE(LOG_ERROR, szLogContent);
	
	// // 打印第三条日志
	// snprintf(szLogContent, sizeof(szLogContent)-1, "The Warn log info!");
	// WRITELOGFILE(LOG_WARN, szLogContent);

	// // 打印第四条日志
	// snprintf(szLogContent, sizeof(szLogContent)-1, "The Info log info!");
	// WRITELOGFILE(LOG_INFO, szLogContent);
	
	// // 打印第五条日志
	// snprintf(szLogContent, sizeof(szLogContent)-1, "The Trace log info!");
	// WRITELOGFILE(LOG_TRACE, szLogContent);
	
	// // 打印第六条日志
	// snprintf(szLogContent, sizeof(szLogContent)-1, "The Debug log info!");
	// WRITELOGFILE(LOG_DEBUG, szLogContent);
	
	// // 打印第七条日志
	// snprintf(szLogContent, sizeof(szLogContent)-1, "The All log info!");
	// WRITELOGFILE(LOG_ALL, szLogContent);
	
	GetEmployeeInfo();   // 获取并打印员工信息 

  return 0;
}