#include <mutex>
#include <typeinfo>
#include <unordered_map>
#include <vector>
// #include <iosfwd>
#include <iostream>
#include <sstream>
#include <string>
#include "object.h"

struct TypeInfo {
  TypeInfo(uint32_t idx, uint32_t pidx, std::string const n)
      : index(idx), parent_index(pidx), name(n) {}

  uint32_t index{0};
  uint32_t parent_index{0};
  std::string name;
};

struct TypeContext {
  // NOTE: this is a relatively slow path for child checking
  // Most types are already checked by the fast-path via reserved slot checking.
  bool DerivedFrom(uint32_t child_tindex, uint32_t parent_tindex) {
    // invariance: child's type index is always bigger than its parent.
    if (child_tindex < parent_tindex) return false;
    if (child_tindex == parent_tindex) return true;
    {
      std::lock_guard<std::mutex> lock(mutex_);
      // CHECK_LT(child_tindex, type_table_.size());
      while (child_tindex > parent_tindex) {
        child_tindex = type_table_[child_tindex].parent_index;
      }
    }
    return child_tindex == parent_tindex;
  }

  std::string const& GetTypeKey(uint32_t tidx) {
    return type_table_.at(tidx).name;
  }

  bool FindTypeIndex(char const* key, uint32_t* index) {
    auto iter = type_key2index_.find(key);
    if (iter == type_key2index_.end()) {
      return false;
    }

    *index = iter->second;
    return true;
  }

  uint32_t GetOrAllocTypeIndex(char const* key, uint32_t parent_tidx) {
    std::unique_lock<std::mutex> lock{mutex_};

    uint32_t find_index;
    if (FindTypeIndex(key, &find_index)) {
      return find_index;
    }
    uint32_t allocated_tidx = type_counter_;
    type_counter_.fetch_add(1);
    // CHECK_GT(allocated_tidx, parent_tidx);

    type_table_.emplace_back(allocated_tidx, parent_tidx, key);
    type_key2index_.insert({key, allocated_tidx});
    return allocated_tidx;
  }

  static TypeContext* Global() {
    static TypeContext inst;
    return &inst;
  }

 private:
  TypeContext() { type_table_.emplace_back(0, 0, "Object"); }

  std::mutex mutex_;
  std::atomic<uint32_t> type_counter_{1};
  std::vector<TypeInfo> type_table_;
  std::unordered_map<std::string, uint32_t> type_key2index_;
};

bool Object::DerivedFrom(uint32_t child_tindex, uint32_t parent_tindex) {
  return TypeContext::Global()->DerivedFrom(child_tindex, parent_tindex);
}

bool Object::FindTypeIndex(char const* type_key, uint32_t* index) {
  return TypeContext::Global()->FindTypeIndex(type_key, index);
}

uint32_t Object::GetOrAllocTypeIndex(const std::string & type_key, uint32_t parent_tidx) {
  return TypeContext::Global()->GetOrAllocTypeIndex(type_key.c_str(), parent_tidx);
}

char const* Object::GetTypeKey() const {
  return TypeContext::Global()->GetTypeKey(this->type_index_).c_str();
}

char const* Object::GetTypeKey(uint32_t idx) {
  return TypeContext::Global()->GetTypeKey(idx).c_str();
}

void Object::IncRef() { ref_count.increment(); }

void Object::DecRef() {
  if (ref_count.decrement() == 0) {
    if (this->deleter_ != nullptr) {
      (*this->deleter_)(this);
    }
  }
}


template <typename FType>
class NodeFunctor;

template <typename R, typename... Args>
class NodeFunctor<R(const ObjectRef& n, Args...)> {
 private:
  /*! \brief internal function pointer type */
  typedef R (*FPointer)(const ObjectRef& n, Args...);
  /*! \brief refer to itself. */
  using TSelf = NodeFunctor<R(const ObjectRef& n, Args...)>;
  /*! \brief internal function table */
  std::vector<FPointer> func_;

 public:
  /*! \brief the result type of this functor */
  using result_type = R;
  /*!
   * \brief Whether the functor can dispatch the corresponding Node
   * \param n The node to be dispatched
   * \return Whether dispatching function is registered for n's type.
   */
  bool can_dispatch(const ObjectRef& n) const {
    uint32_t type_index = n->type_index_;
    return type_index < func_.size() && func_[type_index] != nullptr;
  }
  /*!
   * \brief invoke the functor, dispatch on type of n
   * \param n The Node argument
   * \param args The additional arguments
   * \return The result.
   */
  R operator()(const ObjectRef& n, Args... args) const {
    // CHECK(can_dispatch(n))
    //     << "NodeFunctor calls un-registered function on type "
    //     << n->GetTypeKey();
    return (*func_[n->type_index_])(n, std::forward<Args>(args)...);
  }
  /*!
   * \brief set the dispacher for type TNode
   * \param f The function to be set.
   * \tparam TNode the type of Node to be dispatched.
   * \return reference to self.
   */
  template <typename TNode>
  TSelf& set_dispatch(FPointer f) {  // NOLINT(*)
    uint32_t tindex = TNode::TypeIndex();
    if (func_.size() <= tindex) {
      func_.resize(tindex + 1, nullptr);
    }
    CHECK(func_[tindex] == nullptr)
        << "Dispatch for " << TNode::GetTypeKey(tindex) << " is already set";
    func_[tindex] = f;
    return *this;
  }
  /*!
   * \brief unset the dispacher for type TNode
   *
   * \tparam TNode the type of Node to be dispatched.
   * \return reference to self.
   */
  template <typename TNode>
  TSelf& clear_dispatch() {  // NOLINT(*)
    uint32_t tindex = TNode::RuntimeTypeIndex();
    CHECK_LT(tindex, func_.size()) << "clear_dispatch: index out of range";
    func_[tindex] = nullptr;
    return *this;
  }
};

/*! \brief A printer class to print the AST/IR nodes. */
class ReprPrinter {
 public:
  /*! \brief The output stream */
  std::ostream& stream;
  /*! \brief The indentation level. */
  int indent{0};

  explicit ReprPrinter(std::ostream& stream)  // NOLINT(*)
      : stream(stream) {}

  /*! \brief The node to be printed. */
  void Print(const ObjectRef& node);
  /*! \brief Print indent to the stream */
  void PrintIndent();
  // Allow registration to be printer.
  using FType = NodeFunctor<void(const ObjectRef&, ReprPrinter*)>;
  static FType& vtable();
};
void ReprPrinter::Print(const ObjectRef& node) {
  static const FType& f = vtable();
  if (!node.defined()) {
    stream << "(nullptr)";
  } else {
    if (f.can_dispatch(node)) {
      f(node, this);
    } else {
      // default value, output type key and addr.
      stream << node->GetTypeKey() << "(" << node.get() << ")";
    }
  }
}

void ReprPrinter::PrintIndent() {
  for (int i = 0; i < indent; ++i) {
    stream << ' ';
  }
}

ReprPrinter::FType& ReprPrinter::vtable() {
  static FType inst;
  return inst;
}


inline std::ostream& operator<<(std::ostream& os, const ObjectRef& n) {
  ReprPrinter(os).Print(n);
  return os;
}
std::string ObjectRef::to_string() const {
  std::ostringstream os;
  os << *this;
  return os.str();
}

void ObjectRef::dump() const {
  std::cerr << to_string() << std::endl;
}