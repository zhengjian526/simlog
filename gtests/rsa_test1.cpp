#include "gtest/gtest.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include<string.h>
#include <math.h>
#include<algorithm>
#include <time.h>
using namespace std;

// typedef long long ll;
// int e, d, n;

// int gcd(int a, int b)  //求最大公约数
// {
//     int c = 0;
//     if(a<b) swap(a,b);
//     c = b;
//     do
//     {
//         b = c;
//         c = a%b;
//         a = b;
//     }
//     while (c != 0);
//     return b;
// }

// int PrimarityTest(int a, int i) //判断i是否是素数
// {
//     int flag=0;
//     for(a;a<i;a++)
//     {
//         if(i%a==0)
//         {
//             flag=1;
//             break;
//         }
//     }
//     if(flag) return 0;
//     return 1;
//     // complete this part
// }

// int ModularExponention(int a, int b, int n)  //求a^bmodn
// {
//     int y;

//     /*使用二进制平方乘法计算 pow(a,b) % n*/
//     y=1;

//     while(b != 0)
//     {
//         /*对于b中的每个1，累加y*/

//         if(b & 1)
//             y = (y*a) % n;

//         /*对于b中的每一位，计算a的平方*/
//         a = (a*a) % n;

//         /*准备b中的下一位*/
//         b = b>>1;
//     }

//     return y;
//     // complete this part
// }

// void extgcd(ll a,ll b,ll& d,ll& x,ll& y) //获取(1/a)modb得结果
// {
//     if(!b)
//     {
//         d=a;
//         x=1;
//         y=0;
//     }
//     else
//     {
//         extgcd(b,a%b,d,y,x);
//         y-=x*(a/b);
//     }
// }

// int ModularInverse(int a,int b)  //获取(1/a)modb得结果
// {
//     ll d,x,y;
//     extgcd(a,b,d,x,y);
//     return d==1?(x+b)%b:-1;
//     // complete this part
// }

// void KeyGeneration()  //获取公钥密钥
// {
//     int p, q;
//     int phi_n;

//     do
//     {
//         do
//             p = rand();
//         while (p % 2 == 0);

//     }
//     while (!PrimarityTest(2, p));

//     do
//     {
//         do
//             q = rand();
//         while (q % 2 == 0);
//     }
//     while (!PrimarityTest(2, q));

//     n = p * q;
//     phi_n = (p - 1) * (q - 1);

//     do
//         e = rand() % (phi_n - 2) + 2; // 1 < e < phi_n
//     while (gcd(e, phi_n) != 1);

//     d = ModularInverse(e,phi_n);
// }

// void Encryption(int value, FILE* out)  //加密
// {
//     int cipher;
//     cipher = ModularExponention(value, e, n);
//     fprintf(out, "%d ", cipher);
// }

// void Decryption(int value, FILE* out)  //解密
// {
//     int decipher;
//     decipher = ModularExponention(value, d, n);
//     fprintf(out, "%c", decipher);
// }

//================================================
inline int gcd(int a,int b){
    int t;
    while(b){
        t=a;
        a=b;
        b=t%b;
    }
    return a;
}
bool prime_w(int a,int b){
    if(gcd(a,b) == 1)
    return true;
    else
    return false;
}
inline int mod_inverse(int a,int r){
    int b=1;
    while(((a*b)%r)!=1){
        b++;
        if(b<0){
            printf("error ,function can't find b ,and now b is negative number");
            return -1;
        }
    }
    return b;
} 
inline bool prime(int i){ 
    if(i<=1)
        return false;    
    for(int j=2;j<i;j++){
        if(i%j==0)return false;
    }
    return true;
}
void secret_key(int* p, int *q){
    int s = time(0);
    srand(s);
    do{ 
        *p = rand()%50+1;
    }while(!prime(*p));
    do{
        *q = rand()%50+1;
    }while( p==q || !prime(*q ));
}
int getRand_e(int r){
    int e = 2;
    while( e<1||e>r|| !prime_w(e,r) ){
        e++;
        if(e<0){
            printf("error ,function can't find e ,and now e is negative number");
            return -1;
        }
    }
    return e;
}
int rsa(int a,int b,int c){ 
    int aa = a,r=1;
    b=b+1;
    while(b!=1){
        r=r*aa;
        r=r%c;
        b--;
    }
    return r;
}
int getlen(char *str){
    int i=0;
    while(str[i]!='\0'){
        i++;
        if(i<0)return -1;
    }
    return i;
}


int main(int argc, char** argv){

    FILE *fp;
    fp = fopen("text.txt","w");
    for(int i=2;i<=65535;i++)
        if(prime(i))
            fprintf(fp,"%d ",i);
    fclose(fp);
    int p , q , N , r , e , d;
    p=0,q=0,N=0,e=0,d=0;
    secret_key(&p,&q);
    N = p*q;
    r = (p-1)*(q-1);
    e = getRand_e(r);
    d = mod_inverse(e,r);
    cout<<"N:"<<N<<'\n'<<"p:"<<p<<'\n'<<"q:"<<q<<'\n'<<"r:"<<r<<'\n'<<"e:"<<e<<'\n'<<"d:"<<d<<'\n';
    char mingwen,jiemi;
    int miwen;
    char mingwenStr[1024],jiemiStr[1024];
    int mingwenStrlen;
    int *miwenBuff;
    cout<<"\n\n输入明文：";
    cin >> mingwenStr;    
    mingwenStrlen = getlen(mingwenStr);
    miwenBuff = (int*)malloc(sizeof(int)*mingwenStrlen);
    for(int i = 0;i<mingwenStrlen;i++){
        miwenBuff[i] = rsa((int)mingwenStr[i],e,N);
    }    
    for(int i = 0;i<mingwenStrlen;i++){
        jiemiStr[i] = rsa(miwenBuff[i],d,N);
    }
    jiemiStr[ mingwenStrlen ] = '\0';    
    cout<<"明文："<<mingwenStr<<'\n'<<"明文长度："<<mingwenStrlen<<'\n';    
    cout<<"密文：";
    for(int i = 0;i<mingwenStrlen;i++)
        cout<<miwenBuff[i]<<" ";
    cout<<'\n';
    cout<<"解密："<<jiemiStr<<'\n';         
    return 0;



    // FILE* inp, * out;
    // char filepath[15], filename[100];

    // strcpy(filepath, "./");  //文件路径

    // sprintf(filename, "%s%s", filepath, "cipher.txt");
    // out = fopen(filename, "w+");  //打开文件
    // fclose(out);
    // sprintf(filename, "%s%s", filepath, "decipher.txt");
    // out = fopen(filename, "w+");  //打开文件
    // fclose(out);

    // KeyGeneration();  //获取公钥密钥

    // sprintf(filename, "%s%s", filepath, "plain.txt");
    // inp = fopen(filename, "r+");  //读取原文件
    // if (inp == NULL)
    // {
    //     printf("Error opening Source File.\n");
    //     exit(1);
    // }

    // sprintf(filename, "%s%s", filepath, "cipher.txt");
    // out = fopen(filename, "w+");
    // if (out == NULL)
    // {
    //     printf("Error opening Destination File.\n");
    //     exit(1);
    // }

    // // encryption starts
    // while (1)
    // {
    //     char ch = getc(inp);  //读取文件字符，一个字符一个字符的读取输出
    //     if (ch == -1)
    //         break;
    //     int value = toascii(ch);  //toascii将字符转化成对应ascall值
    //     Encryption(value, out);  //加密输出
    // }

    // fclose(inp);
    // fclose(out);

    // // decryption starts
    // sprintf(filename, "%s%s", filepath, "cipher.txt");
    // inp = fopen(filename, "r");
    // if (inp == NULL)
    // {
    //     printf("Error opening Cipher Text.\n");
    //     exit(1);
    // }

    // sprintf(filename, "%s%s", filepath, "decipher.txt");
    // out = fopen(filename, "w+");
    // if (out == NULL)
    // {
    //     printf("Error opening File.\n");
    //     exit(1);
    // }

    // while (1)
    // {
    //     int cip;
    //     if (fscanf(inp, "%d", &cip) == -1)
    //         break;
    //     Decryption(cip, out);  //解密
    // }
    // fclose(out);

    // return 0;

//   testing::InitGoogleTest(&argc, argv);

//   return RUN_ALL_TESTS();
}