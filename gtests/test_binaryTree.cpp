#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;


struct Node
{
    int data;
    Node *lchild, *rchild;
};
struct Tree
{
    Node* root;
    int length;
};

Node* GetNewNode(int val)
{
    Node* node = new Node();
    node->data = val;
    node->lchild = node->rchild = nullptr;
    return node;
}
Tree* GetNewTree()
{
    Tree* tree = new Tree();
    tree->root = nullptr;
    tree->length = 0;
    return tree;
}
Node* InsertNode(Node* root, int val, int* flag )
{
    if (root == nullptr)
    {
        *flag = 1;
        return GetNewNode(val);
    }
    if (root->data == val) return root;
    if(root->data > val) root->lchild = InsertNode(root->lchild,val,flag);
    if(root->data < val) root->rchild = InsertNode(root->rchild,val,flag);
    return root;
}
void Insert(Tree* tree, int val)
{
    if (tree == nullptr) return;
    int flag = 0;
    tree->root = InsertNode(tree->root,val,&flag);
    tree->length += flag;
    return;
}

void ClearNode(Node* root)
{
    if (root == nullptr) return;
    ClearNode(root->lchild);
    ClearNode(root->rchild);
    delete root;
    return;
}
void ClearTree(Tree* tree)
{
    if (tree == nullptr) return;
    ClearNode(tree->root);
    delete(tree);
    return ;
}
void PreOrderNode(Node* root)
{
    if(root == nullptr) return;
    cout << root->data << " ";
    PreOrderNode(root->lchild);
    PreOrderNode(root->rchild);
    return;
}
void PreOrder(Tree* tree)
{
    if(tree == nullptr) return;
    cout << "pre_order: ";
    PreOrderNode(tree->root);
    cout << endl;
    return;
}
void InOrderNode(Node* root)
{
    if(root == nullptr) return;
    InOrderNode(root->lchild);
    cout << root->data << " ";
    InOrderNode(root->rchild);
    return;
}
void InOrder(Tree* tree)
{
    if(tree == nullptr) return;
    cout << "in_order: ";
    InOrderNode(tree->root);
    cout << endl;
    return;
}

void PostOrderNode(Node* root)
{
    if(root == nullptr) return;
    PostOrderNode(root->lchild);
    PostOrderNode(root->rchild);
    cout << root->data << " ";
    return;
}
void PostOrder(Tree* tree)
{
    cout << "post_order: ";
    PostOrderNode(tree->root);
    cout << endl;
    return;
}
void OutputNode(Node* root){
    if(root == nullptr) return;
    cout << root->data;
    if (root->lchild == nullptr && root->rchild == nullptr) return ;
    cout << "(" ;
    OutputNode(root->lchild);
    cout << ",";
    OutputNode(root->lchild);
    cout << ")";
    return;
}

void Output(Tree* tree){
    if(tree == nullptr) return;
    cout << "tree(" << tree->length << "): " ;
    OutputNode(tree->root);
    cout << endl;
    return;
}


int main(int argc, char** argv){
    srand(time(0));
    Tree* tree = GetNewTree();
    #define MAX_OP 10
    for (size_t i = 0; i < MAX_OP; i++)
    {
        int val = rand() % 100;
        Insert(tree,val);
        Output(tree);
    }
    PreOrder(tree);
    InOrder(tree);
    PostOrder(tree);
    ClearTree(tree);
    
    return 0;

}