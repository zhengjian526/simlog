#include <iostream>
#include <typeinfo>
using namespace std;

#define BEGINS(X) namespace X { // begin of namespace x
#define ENDS(X) } // end of namespace x 

BEGINS(inherit_ctor_order)
class X {
public:
    X* ptr;
    X() {
        cout << typeid(*this).name() << endl;
        ptr = this;
        cout << "ptr::" << ptr << endl;
    }

    virtual void func() { cout << "X func" << endl; }
};

class Y : public X {
public:
    Y() {}
    virtual void func() { cout << "Y func" << endl; }
};

int main() {
    X x;
    Y y;
    x.ptr->func();
    y.ptr->func();
    return 0;
}
ENDS(inherit_ctor_order)
BEGINS(placement_new)
class AA
{
private:
    /* data */
public:
    int val = 11;
    AA(/* args */);
    ~AA();
};

AA::AA(/* args */)
{
}

AA::~AA()
{
}

int main(){
    char* addr = new char[sizeof(AA)];
    AA* pp = new(addr) AA; //原地构造
    cout << pp->val << endl;
}
ENDS(placement_new)

BEGINS(try_catch)
class EE
{
private:
    /* data */
public:
    int val = 9;
    EE(/* args */);
    ~EE();
};

EE::EE(/* args */)
{
}

EE::~EE()
{
}

int main(){
    // try
    // {
    //     cout << "try process 1" << endl;
    //     throw(1);
    //     cout << "try process 2" << endl;
    // }
    // catch(const int e)
    // {
    //     cout << "catch exception" << endl;
    // }
    // cout << "after try catch" << endl;

}
ENDS(try_catch)


int main(){
    // inherit_ctor_order::main();
    // placement_new::main();
    try_catch::main();
    return 0;
}