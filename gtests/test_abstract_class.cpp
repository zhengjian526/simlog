#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
using namespace std;
struct base
{
  static int a;
  static int print(){
    return 10;
  }
};
int base::a = 0;
struct layer : base
{
  int b;
  void print(){cout << b; }
};



int main(int argc, char** argv){
  // base aa;
  // aa.a = aa.print();
  // cout << aa.a << endl;
  // layer t;
  // t.b = 11;
  // base* p = &t;
  // layer* s = (layer*)p;
  // cout << s->b << endl;
  auto w = std::make_shared<int>(12);
  {
      auto w2 = w;
      cout << w.use_count() << endl;  // 2
      cout << *w << endl;
      cout << w2.use_count() << endl;  // 2
      cout << *w2 << endl;      
      int* p = w.get();
      *p = 244;
      cout << w.use_count() << endl;  // 2
      cout << *w << endl;
      cout << w2.use_count() << endl;  // 2
      cout << *w2 << endl;
  }  
  cout << w.use_count() << endl;  // 1
  return 0;

}