#include <functional>
#include <iostream>
#include "cpplang.hpp"


BEGIN_NAMESPACE(function_and_bind)
using namespace std;
using std::placeholders::_1;

int main(){
    std::vector<int> coll = {1,2,3,4,5,6,7,8,9,10};
    int count = std::count_if(coll.begin(),coll.end(),std::bind(std::less<int>(),10,_1));
    cout << "-----:" << count << endl;
}

END_NAMESPACE(function_and_bind)

BEGIN_NAMESPACE(multi_thread)
#include <thread>
#include <mutex>
#include <chrono>
class Complex_dead_lock
{
private:
    /* data */
public:
    std::mutex mutex;//死锁
    int i;
    Complex_dead_lock(/* args */):i(3){}
    ~Complex_dead_lock(){};
    void mul(int x){
        std::lock_guard<std::mutex> lock(mutex);
        i *= x;
    }
    void div(int x){
        std::lock_guard<std::mutex> lock(mutex);
        i /= x;
    }
    void both(int x, int y){
        std::lock_guard<std::mutex> lock(mutex);
        mul(x);
        div(y);
    }
};
class Complex_recursive
{
private:
    /* data */
public:
    std::recursive_mutex rec_mutex;//死锁
    int i;
    Complex_recursive(/* args */):i(3){}
    ~Complex_recursive(){};
    void mul(int x){
        std::lock_guard<std::recursive_mutex> lock(rec_mutex);
        i *= x;
    }
    void div(int x){
        std::lock_guard<std::recursive_mutex> lock(rec_mutex);
        i /= x;
    }
    void both(int x, int y){
        std::lock_guard<std::recursive_mutex> lock(rec_mutex);
        mul(x);
        div(y);
    }
};
int main(){
    //死锁
    // Complex_dead_lock com;
    // std::cout << "-------------1" << std::endl;
    // com.both(12,3);
    // std::cout << "-------------2" << std::endl;
    // std::cout << com.i << std::endl;
    //递归互斥量 不死锁
    Complex_recursive com;
    std::cout << "-------------1" << std::endl;
    com.both(12,3);
    std::cout << "-------------2" << std::endl;
    std::cout << com.i << std::endl;
    return 0;
}
END_NAMESPACE(multi_thread)
BEGIN_NAMESPACE(move_and_forward)
using namespace std;
template<typename T>
void PrintT(T& t){
    cout << "lvalue" << endl;
}
template<typename T>
void PrintT(T&& t){
    cout << "rvalue" << endl;
}
template<typename T>
void TestForward(T&& t){
    PrintT(t);
    PrintT(std::forward<T>(t));
    PrintT(std::move(t));
}
int main(){
    TestForward(1);
    int x = 2;
    TestForward(x);
    TestForward(std::forward<int>(x));
}

END_NAMESPACE(move_and_forward)

class A{
    public:
    int a;
    A(){std::cout << "ctor" << std::endl;}
    ~A(){std::cout << "dtor" << std::endl;}
    A(int a_):a(a_){std::cout << "ctor1" << std::endl;}
};

int main(){
    // function_and_bind::main();
    // multi_thread::main();
    // move_and_forward::main();
    std::shared_ptr<A> pp1 = std::make_shared<A>(1);
    // std::shared_ptr<A> pp2 = std::make_shared<A>(2);
    std::shared_ptr<A> pp3 = pp1;
    // pp1 = pp2;
    pp3.reset();
    std::cout << "------" << pp3.get() << std::endl;
    pp3 = nullptr;
    std::cout << "------" << pp3.get() << std::endl;
    std::cout << "------" << std::endl;
    return 0;
}