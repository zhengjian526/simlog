#include "gtest/gtest.h"
#include <math.h>
using namespace std;

template <typename T>
auto multiply(T x, T y)->decltype(x*y)
{
    return x*y;
}

TEST(TestLog, test_decltype)
{
    int nums[] = {1,2,3,4};
    vector<int> vec(nums,nums+4);
    vector<int>::iterator it;

    for(it=vec.begin();it!=vec.end();it++)
        cout<<*it<<" ";
    cout<<endl;


    using nullptr_t = decltype(nullptr);
    nullptr_t  nu;
    int * p =NULL;
    if(p==nu)
        cout<<"NULL"<<endl;


    typedef decltype(vec.begin()) vectype;

    for(vectype i=vec.begin();i!=vec.end();i++)
        cout<<*i<<" ";
    cout<<endl;

    /**
     * 匿名结构体
     */
    struct
    {
        int d ;
        double b;
    }anon_s;

    decltype(anon_s) as; // 定义了一个上面匿名的结构体

    cout<<multiply(11,2)<<endl;

}

int main(int argc, char** argv){

  int && rRef = pow(5,2);
  cout << rRef << endl;
  int & rRef2 = rRef;
  cout << rRef2 << endl;

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}