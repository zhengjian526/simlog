#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include "cpplang.hpp"
using namespace std;
BEGIN_NAMESPACE(test1)
template<typename T>
class SmartPointer {
private:
    T* _ptr;
    size_t* _count;
public:
    SmartPointer(T* ptr = nullptr) :
            _ptr(ptr) {
        if (_ptr) {
            _count = new size_t(1);
        } else {
            _count = new size_t(0);
        }
    }

    SmartPointer(const SmartPointer& ptr) {
        if (this != &ptr) {
            this->_ptr = ptr._ptr;
            this->_count = ptr._count;
            (*this->_count)++;
            cout << "after  construct:" <<*this->_count <<endl;
        }
    }

    SmartPointer& operator=(const SmartPointer& ptr) {
        if (this->_ptr == ptr._ptr) {
            return *this;
        }

        if (this->_ptr) {
            (*this->_count)--;
            if (this->_count == 0) {
                delete this->_ptr;
                delete this->_count;
            }
        }

        this->_ptr = ptr._ptr;
        this->_count = ptr._count;
        (*this->_count)++;
        return *this;
    }

    T& operator*() {
        assert(this->_ptr == nullptr);
        return *(this->_ptr);

    }

    T* operator->() {
        assert(this->_ptr == nullptr);
        return this->_ptr;
    }

    ~SmartPointer() {
        cout << "before delete:" <<*this->_count <<endl;
        (*this->_count)--;
        cout << "after delete:" <<*this->_count <<endl;
        if (*this->_count == 0) {
            delete this->_ptr;
            delete this->_count;
        }
    }

    size_t use_count(){
        return *this->_count;
    }
};

int main(int argc, char** argv){
        SmartPointer<int> sp(new int(10));
        std::cout << "----==-:"<< sp.use_count() << std::endl;
        SmartPointer<int> sp2(sp);
        std::cout << "----==-:"<< sp.use_count() << std::endl;
        std::cout << "----==-:"<< sp2.use_count() << std::endl;
        SmartPointer<int> sp3(new int(20));
        sp2 = sp3;
        std::cout << sp.use_count() << std::endl;
        std::cout << sp2.use_count() << std::endl;
        std::cout << sp3.use_count() << std::endl;
        //对比
        shared_ptr<int> ss(new int(10));
        shared_ptr<int> ss2(ss);
        shared_ptr<int> ss3(new int(20));
        ss2 = ss3;
        std::cout << ss.use_count() << std::endl;
        std::cout << ss2.use_count() << std::endl;
        std::cout << ss3.use_count() << std::endl;
  return 0;
}
END_NAMESPACE(test1)

BEGIN_NAMESPACE(test_weak_ptr)
struct A;
struct B;
struct A {
        std::weak_ptr<B> bptr;
        ~A() { cout << "A is deleted!" << endl; }
};
struct B {
        std::shared_ptr<A> aptr; // 改为weak_ptr
        ~B() { cout << "B is deleted!" << endl; }
};
void TestPtr()
{
    {
        std::shared_ptr<A> ap(new A);
        std::shared_ptr<B> bp(new B);
        ap->bptr = bp;
        bp->aptr = ap;
    }  // Objects should be destroyed.
}
int main(){
    TestPtr();
}
END_NAMESPACE(test_weak_ptr)

int main(){
    test_weak_ptr::main();
    return 0;
}