#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <functional>

using namespace std;
#define BEGINS(x) namespace x{ 
#define ENDS(x) } 
BEGINS(test1)
void test1(){
    int val ;
    // auto const_val_lambda = [=](){val = 3;};

    auto mutable_val_lambda = [=]() mutable {val = 3;};
    cout << "-------: " << val << endl;

    auto const_ref_lambda = [&] {val = 3;};
    cout << "-------: " << val << endl;

    auto const_param_lambda = [&](int v) {v=3;};
    const_param_lambda(val);
    cout << "-------: " << val << endl;
}
void test2(){
    int boys = 4, girls = 3;
    auto totalChild = [=]()->int{
        cout << boys << "--" << girls << endl;
        return boys + girls;
    };
    cout << boys << "--" << girls << endl;
    boys++;
    girls++;

    auto totalChild1 = [&]()->int{
        cout << boys << "--" << girls << endl;
        return boys + girls;
    };
    cout << totalChild() <<endl;
    cout << totalChild1() <<endl;
}
// int d = 0;
// int TryCapture(){
//     auto ill_lambda = [d]{};
// }
struct HowManyBytes
{
    char a ;
    int  b ;
};


int main(int argc, char** argv){
  cout << sizeof(HowManyBytes) <<endl;
  cout << offsetof(HowManyBytes,a) <<endl;
  cout << offsetof(HowManyBytes,b) <<endl;
  return 0;

}
ENDS(test1)

BEGINS(test2)
void my_square(int x)
{
    cout << x*x << endl;
}

void case1()
{
    auto pfunc = &my_square;

    (*pfunc)(3);
    pfunc(3);

    auto func = [](int x)
    {
        cout << x*x << endl;
    };

    func(3);
}

void case2()
{
    int n = 10;

    auto func = [=](int x)
    {
        cout << x*n << endl;
    };

    func(3);
}

void case3()
{
    auto f1 = [](){};

    auto f2 = []()
        {
            cout << "lambda f2" << endl;

            auto f3 = [](int x)
            {
                return x*x;
            };// lambda f3

            cout << f3(10) << endl;
        };  // lambda f2

    f1();
    f2();

    //f1 = f2;

    vector<int> v = {3, 1, 8, 5, 0};

    cout << *find_if(begin(v), end(v),
                [](int x)
                {
                    return x >= 5;
                }
            )
         << endl;
}

void case4()
{
    int x = 33;

    auto f1 = [=]()
    {
        //x += 10;
        cout << x << endl;
    };

    auto f2 = [&]()
    {
        x += 10;
    };

    auto f3 = [=, &x]()
    {
        x += 20;
    };

    f1();
    f2();
    cout << x << endl;
    f3();
    cout << x << endl;
}

class DemoLambda final
{
public:
    DemoLambda() = default;
   ~DemoLambda() = default;
private:
    int x = 0;
public:
    auto print()
    {
        //auto f = [=]()

        return [this]()
        {
            cout << "member = " << x << endl;
        };

    }
};

void case5()
{
    DemoLambda obj;

    auto f = obj.print();

    f();
}

void case6()
{
    auto f = [](const auto& x)
    {
        return x + x;
    };

    cout << f(3) << endl;
    cout << f(0.618) << endl;

    string str = "matrix";
    cout << f(str) << endl;
}

// demo for function + lambda
class Demo final
{
public:
    using func_type = std::function<void()>;
public:
    func_type print = [this]()
    {
        cout << "value = " << m_value << endl;
        cout << "hello function+lambda" << endl;
    };
private:
    int m_value = 10;
};

int main()
{
    // case1();
    // case2();
    // case3();
    // case4();
    case5();
    case6();

    Demo d;
    d.print();

    cout << "lambda demo" << endl;
}
ENDS(test2)
int main(){
    test2::main();

    return 0;   
}