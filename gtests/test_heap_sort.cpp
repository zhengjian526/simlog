#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;

#define swap(a, b){\
    __typeof(a) __tmp = a;\
    a = b;\
    b = __tmp;\
}
void downUpdate(int* arr, int n, int ind){
    while ((ind << 1) <= n)
    {
        int tmp = ind, l = ind << 1, r = (ind << 1) + 1;
        if (arr[l] > arr[tmp]) tmp = l;
        if (r <= n && arr[r] > arr[tmp]) tmp = r;
        if (tmp == ind) break;
        swap(arr[ind],arr[tmp]);
        ind = tmp;
    }
    return;
}
void heap_sort(int* arr, int n){
    arr -= 1; //传进来的数组下标从0开始,这样操作后arr[1]实际访问的就是arr[0]
    //线性建堆法,从最后一个非叶子节点到第一个节点依次进行向下调整
    for (size_t i = n >> 1; i >= 1; i--)
    {
        downUpdate(arr, n, i);
    }

    for (size_t i = n; i >= 1; i--)
    {
        swap(arr[i], arr[1]);
        downUpdate(arr, i-1, 1);
    }
    return;
} 
void output(int* arr, int n){
    for (size_t i = 0; i < n; i++)
    {
        i && cout << " ";
        cout << arr[i];
    }
    cout << endl;
}

int main(int argc, char** argv){
    srand(time(0));
    #define MAX_OP 20
    int* arr =  new int[MAX_OP];
    for (size_t i = 0; i < MAX_OP; i++)
    {
        arr[i] = rand() % 100;
    }
    output(arr, MAX_OP);
    heap_sort(arr,MAX_OP);
    output(arr, MAX_OP);
    return 0;
}