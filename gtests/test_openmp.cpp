#include "gtest/gtest.h"
#include "omp.h"
#include <cmath>
using namespace std;

TEST(TestLog, case_0){
    double dur;
    clock_t start,end;
    start = clock();

     const int size = 1000000;
    double sinTable[size];
    
    // #pragma omp parallel for num_threads(8)
    // #pragma omp simd
    #pragma omp target teams distribute parallel for map(from:sinTable[0:1000000])
    for(int n=0; n<size; ++n)
      sinTable[n] = std::sin(2 * M_PI * n / size);
    end = clock();
    dur = (double)(end - start);
    cout << "Sim run use time:" << dur/CLOCKS_PER_SEC << endl;

}


// TEST(TestLog, case_1)
// {
//     double dur;
//     clock_t start,end;
//     start = clock();
//     // omp_set_num_threads(10);
//     // #pragma omp parallel for num_threads(5)
//     for (int i = 0; i <= 100; i++){
//         cout << "threads::" << omp_get_num_threads() <<endl;
//         std::cout << i << std::endl;
//     }
//     end = clock();
//     dur = (double)(end - start);
//     cout << "Sim run use time:" << dur/CLOCKS_PER_SEC << endl;

// }



int main(int argc, char** argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}