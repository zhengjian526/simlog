#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;
//===============================quick find===========================================
// struct UnionSet
// {
//     int* color;
//     int n;
// };

// UnionSet* Init(int n){
//     UnionSet* u = new UnionSet();
//     u->color = new int[n+1];
//     u->n = n;
//     for (size_t i = 1; i <= n; i++)
//     {
//         u->color[i] = i;
//     }
//     return u;
// }
// void Clear(UnionSet* u){
//     if (u == nullptr ) return;
//     delete u->color;
//     delete u;
//     return;
// }

// int find(UnionSet* u, int x){
//     return u->color[x];
// }
// int merge(UnionSet* u, int a, int b){
//     if (find(u,a) == find(u,b)) return 0;
//     int c_a = u->color[a];
//     for (size_t i = 1; i <= u->n; i++)
//     {
//         if (u->color[i] != c_a) continue;
//         u->color[i] = u->color[b];
//     }
//     return 1;
// }
//===============================quick union===========================================
// struct UnionSet
// {
//     int* father;
//     int n;
// };

// UnionSet* Init(int n){
//     UnionSet* u = new UnionSet();
//     u->father = new int[n+1];
//     u->n = n;
//     for (size_t i = 1; i <= n; i++)
//     {
//         u->father[i] = i;
//     }
//     return u;
// }
// void Clear(UnionSet* u){
//     if (u == nullptr ) return;
//     delete u->father;
//     delete u;
//     return;
// }

// int find(UnionSet* u, int x){
//     if(u->father[x] == x) return x;
//     return find(u, u->father[x]);
// }
// int merge(UnionSet* u, int a, int b){
//     int fa = find(u, a), fb = find(u, b);
//     if (fa == fb) return 0;
//     u->father[fa] = fb;
//     return 1;
// }
//===============================weighted quick union===========================================
// #define swap(a, b){\
//     __typeof(a) t = a;\
//     a = b; b = t;\
// }

// struct UnionSet
// {
//     int* father;
//     int* size;
//     int n;
// };

// UnionSet* Init(int n){
//     UnionSet* u = new UnionSet();
//     u->father = new int[n+1];
//     u->size = new int[n+1];
//     u->n = n;
//     for (size_t i = 1; i <= n; i++)
//     {
//         u->father[i] = i;
//         u->size[i] = 1;
//     }
//     return u;
// }
// void Clear(UnionSet* u){
//     if (u == nullptr ) return;
//     delete u->father;
//     delete u->size;
//     delete u;
//     return;
// }

// int find(UnionSet* u, int x){
//     if(u->father[x] == x) return x;
//     return find(u, u->father[x]);
// }
// int merge(UnionSet* u, int a, int b){
//     int fa = find(u, a), fb = find(u, b);
//     if (fa == fb) return 0;
//     // u->father[fa] = fb;
//     if (u->size[fa] < u->size[fb]) swap(fa, fb);
//     u->father[fb] = u->father[fa];
//     u->size[fa] += u->size[fb];
//     return 1;
// }

//===============================weighted quick union with path compression===========================================
#define swap(a, b){\
    __typeof(a) t = a;\
    a = b; b = t;\
}

struct UnionSet
{
    int* father;
    int* size;
    int n;
};

UnionSet* Init(int n){
    UnionSet* u = new UnionSet();
    u->father = new int[n+1];
    u->size = new int[n+1];
    u->n = n;
    for (size_t i = 1; i <= n; i++)
    {
        u->father[i] = i;
        u->size[i] = 1;
    }
    return u;
}
void Clear(UnionSet* u){
    if (u == nullptr ) return;
    delete u->father;
    delete u->size;
    delete u;
    return;
}

int find(UnionSet* u, int x){
    if(u->father[x] == x) return x;
    return u->father[x] = find(u, u->father[x]); //路径压缩Here
}
int merge(UnionSet* u, int a, int b){
    int fa = find(u, a), fb = find(u, b);
    if (fa == fb) return 0;
    // u->father[fa] = fb;
    if (u->size[fa] < u->size[fb]) swap(fa, fb);
    u->father[fb] = u->father[fa];
    u->size[fa] += u->size[fb];
    return 1;
}


int main(int argc, char** argv){

    return 0;
}