#include "cereal/types/memory.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/base_class.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/polymorphic.hpp"
#include "cereal/access.hpp"
#include <vector>
#include <fstream>
using namespace std;

struct A
{
    int a;
    virtual int getA() = 0;
    template<class Archive>
    void serialize(Archive &ar)
    {
        ar(a);
    }
    // A(){}
    // ~A(){}
};
struct B : A
{
    int b=9;
    int getA(){}
    template<class Archive>
    void serialize(Archive &ar)
    {
        // 这里序列化了IFly基类
        ar(a, cereal::base_class<A>(this));
    }
    B(){}
    ~B(){}
};
struct C : B
{
    int c=8;
    int getA(){}
    template<class Archive>
    void serialize(Archive &ar)
    {
        // 这里序列化了IFly基类
        ar(c, cereal::base_class<B>(this));
    }
    C(){}
    ~C(){}
};
CEREAL_REGISTER_TYPE(B);
CEREAL_REGISTER_TYPE(C);
struct D : B
{
    int c=8;
    int getA(){}
    template<class Archive>
    void serialize(Archive &ar)
    {
        // 这里序列化了IFly基类
        ar(c, cereal::base_class<B>(this));
    }
    D(){}
    ~D(){}
};


int main(int argc, char** argv){
    {
        // auto m = make_shared<A>();
        // auto m = make_unique<A>();
        auto v = make_unique<vector<shared_ptr<A>>>();
        v->push_back(make_shared<B>());
        v->push_back(make_shared<C>());
        for (auto &itr : *v) { cout << itr->getA() <<endl; }
        cout << "before: " << (static_cast<B*>((*v).at(0).get()))->b << endl;
        cout << "before: " << (static_cast<C*>((*v).at(1).get()))->c << endl;
        // 这里生成二进制数据
        std::ofstream os("out.cereal", std::ios::binary);
        cereal::BinaryOutputArchive archive(os);
        archive(v);
    }
    printf("================================\n");
    {
        auto v = make_unique<vector<shared_ptr<A>>>();
        
        // 对刚刚序列化生成的out.cereal文件进行反序列化
        std::ifstream os("out.cereal", std::ios::binary);
        cereal::BinaryInputArchive archive(os);
        archive(v);
        for (auto &itr : *v) { cout << itr->getA() <<endl; }
        cout << "after: " << (static_cast<B*>((*v).at(0).get()))->b << endl;
        cout << "after: " << (static_cast<C*>((*v).at(1).get()))->c << endl;
    }

    getchar();
    return 0;

}