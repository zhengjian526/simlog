#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;
//异或交换条件是a  != b
#define t_swap(a,b){\
    int tmp = a;\
    a = b;\
    b = tmp;\
}
void outout(int* num, int n){
    for (size_t i = 0; i < n; i++)
    {
        (i == 0) && cout << "[ " ;
        cout << num[i] << " ";
        (i == n-1) && cout << "] " <<endl;
    }
}

void insert_sort(int* num, int n){
    for (size_t i = 1; i < n; i++)
    {
        for (size_t j = i; j > 0 && num[j] < num[j-1]; j--)
        {
            t_swap(num[j-1],num[j]);
        }
    }
    // outout(num,n);
}

void bubble_sort(int* num, int n){
    int flag = 1;
    for (size_t i = 1; i < n && flag; i++)
    {
        flag = 0;
        for (size_t j = 0; j < n-i; j++)
        {
            if(num[j] <= num[j+1]) continue;
            t_swap(num[j],num[j+1]);
            flag = 1;
        }
    }
    // outout(num,n);
    return;
}
void merge_sort(int* num, int l, int r)
{
    if (r - l <= 1)
    {
        if (r - l == 1 && num[r] < num[l])
        {
            t_swap(num[r],num[l]);
        }
        return;
    }
    int mid = (l + r) >> 1;
    merge_sort(num,l,mid);
    merge_sort(num,mid+1,r);
    int* temp = new int[r-l+1];
    int p1 = l, p2 = mid+1, k = 0;
    while (p1 <= mid || p2 <= r)
    {
        if (p2 > r || (p1 <= mid && num[p1] <= num[p2]))
        {
            temp[k++] = num[p1++];
        }
        else
        {
            temp[k++] = num[p2++];
        }
    }
    memcpy(num+l,temp,sizeof(int)*(r-l+1));
    delete temp;
    return;
}

//不稳定排序
void select_sort(int* num, int n)
{
    for (size_t i = 0; i < n-1; i++)
    {
        int idx = i;
        for (int j = i+1; j < n; j++)
        {
            if (num[j] < num[idx]) idx = j;
        }
        t_swap(num[idx], num[i]);
    }
    return;
}
void quick_sort(int* num, int l, int r)
{
    if(l > r) return; 
    int x = l, y = r, z = num[x];
    while (x < y)
    {
        while (x < y && num[y] > z) y--;
        if ( x < y) num[x++] = num[y];
        while (x < y && num[x] < z) x++;
        if (x < y) num[y--] = num[x]; 
    }
    num[x] = z;
    quick_sort(num,l,x-1);
    quick_sort(num,x+1,r);
    return;
}
//优化快速排序

void optimize_quick_sort(int* num, int l, int r)
{
    while (l < r)
    {        
        int x = l, y = r, z = num[( l + r ) >> 1];
        do
        {
            while (num[y] > z) y--;
            while (num[x] < z) x++;
            if (x <= y)
            {
                t_swap(num[x],num[y]);
                x++, y--;
            }
        } while (x <= y);
        optimize_quick_sort(num,l,y);
        l = x;
    }
    return;
}

int main(int argc, char** argv){
    // srand(time(0));
    int arr1[10] = {1,2,4,4,5,11,14,16,16,3,};
    int arr2[10] = {1,2,4,4,5,11,14,16,16,3,};
    int arr3[10] = {1,2,4,4,5,11,14,16,16,3,};
    int arr4[10] = {1,2,4,4,5,11,14,16,16,3,};
    int arr5[10] = {1,2,4,4,5,11,14,16,16,3,};
    int arr6[10] = {1,2,4,4,5,11,14,16,16,3,};
    #define MAX_N 10
    cout << MAX_N << endl;
    insert_sort(arr1,MAX_N);
    outout(arr1,MAX_N);

    bubble_sort(arr2,MAX_N);
    outout(arr2,MAX_N);

    merge_sort(arr3,0,MAX_N-1);
    outout(arr3,MAX_N);

    select_sort(arr4,MAX_N);
    outout(arr4,MAX_N);

    quick_sort(arr5,0,MAX_N-1);
    outout(arr5,MAX_N);

    optimize_quick_sort(arr6,0,MAX_N-1);
    outout(arr6,MAX_N);
    return 0;

}