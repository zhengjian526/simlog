#include "gtest/gtest.h"
#include "expr.h"
using namespace std;


TEST(TestLog, case_main)
{
    // Expr t0 = Expr(5);
    // cout << t0 << endl;
    Expr t1 = Expr("-",Expr(5));
    cout << t1 << endl;
    Expr t9 = Expr(3);
    Expr t10 = Expr(4);
    Expr t2 = Expr("+",t9,t10);
    cout << t2 << endl;
    Expr t = Expr("*",t1,t2);
    cout << t << endl;
    Expr t11 = Expr("*",t,t);
    cout << t11 << endl;
}



int main(int argc, char** argv){
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}