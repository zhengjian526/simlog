#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#define BEGINS(x) namespace x { //namespace x
#define ENDS(x) }
BEGINS(test1)
int main(){
    struct winsize size;
    if(isatty(0) == false){
        printf("1 is not tty\n");
        exit(0);
    }
    if(ioctl(0,TIOCGWINSZ,&size) < 0){
        perror("ioctl");
        exit(1);    
    }
    printf("%d rows, %d columns\n", size.ws_row,size.ws_col);
    return 0;
}
ENDS(test1)

BEGINS(test2)
int main(int argc, char const *argv[])
{
    if(argc < 2){
        printf("usage:cmd fd\n");
        exit(1);
    }
    int flags;
    if((flags = fcntl(atoi(argv[1]),F_GETFL)) <0){
        perror("fcntl get flags");
        exit(1);
    }
    switch (flags & O_ACCMODE)
    {
    case O_RDONLY:
        printf("read only\n");
        break;
    case O_WRONLY:
        printf("write only\n");
        break;
    case O_RDWR:
        printf("read write\n");
        break;
    default:
        printf("valid access mode\n");
        break;
    }
    if(flags & O_APPEND)
        printf(", append\n");
    if(flags & O_NONBLOCK)
        printf(", nonblock\n");
}
ENDS(test2)

BEGINS(test3)
int main(){
    int fd = open("./test.txt",O_RDWR);
    if(fd < 0 ){
        perror("open file");
        exit(1);
    }
    void* p = mmap(nullptr,6,PROT_WRITE,MAP_SHARED,fd,0);
    ((char*)p)[0] = 'A';
    ((char*)p)[2] = 'B';
    ((char*)p)[3] = 'C';
}
ENDS(test3)
int main(int argc, char const *argv[])
{
    // test1::main();
    // test2::main();
    test3::main();
    return 0;
}
