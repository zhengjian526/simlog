#include "gtest/gtest.h"
#include <math.h>
#include<iostream>
#include<fstream>
#include <queue>
// #include<cstring>
#include<vector>
using namespace std;

class Person{
    public:
        Person(){}
        Person(char *name,char *id,int math,int chinese,int english){
            strcpy(Name,name);
            strcpy(Id,id);
            Math=math;
            Chinese=chinese;
            English=english;
            Sum=Math+Chinese+English;
        }

        void display(){
            cout<<Name<<"\t"<<Id<<"\t"<<Math<<"\t"<<Chinese<<"\t"<<English<<"\t"<<Sum<<endl;
        }
    private:
        char Name[20];
        char Id[20];
        int Math;
        int Chinese;
        int English;
        int Sum;
};
class Sales{
private:
		char name[10];
		char id[18];
		int age;
public:
		Sales(char *Name,char *ID,int Age);

		friend	Sales &operator<<(ostream &os,Sales &s);	//重载输出运算符
		friend	Sales &operator>>(istream &is,Sales &s);	//重载输入运算符
};
Sales::Sales(char *Name,char *ID,int Age) {
		strcpy(name,Name);
		strcpy(id,ID);
		age=Age;
}
Sales& operator<<(ostream &os,Sales &s) {
		os<<s.name<<"\t";                 			//输出姓名
		os<<s.id<<"\t";                    			//输出身份证号
		os<<s.age<<endl;                  			//输出年龄
		return s;
}
Sales &operator>>(istream &is,Sales &s) {
		cout<<"输入雇员的姓名，身份证号，年龄"<<endl;  				is>>s.name>>s.id>>s.age;                       	
		return s;
}
TEST(TestLog, test_oper11){
		Sales s1("杜康","214198012111711",40);   		//L1
		cout<<s1;                             			//L2
		cout<<endl;                           				//L3
		cin>>s1;                              				//L4
		cout<<s1;                             				//L5

}
int test_priority_queue() {
    cout<<"============test_priority_queue============="<<endl;
    clock_t timeStart = clock();

    priority_queue<int, deque<int>> c1;
    for (long i = 0; i < 100000; i++) {
        c1.push(i+1);
    }
    cout << "stack.size()= " << c1.size() << endl;
    cout << "stack.top()= " << c1.top() << endl;
    c1.pop();
    cout << "stack.size()= " << c1.size() << endl;
    cout << "stack.top()= " << c1.top() << endl;

    cout << "use deque milli-seconds : " << (clock() - timeStart) << endl;


    priority_queue<int, vector<int>> c2;
    for (long i = 0; i < 100000; i++)
        c2.push(i+1);
    cout << "stack.size()= " << c2.size() << endl;
    cout << "stack.top()= " << c2.top() << endl;
    c2.pop();
    cout << "stack.size()= " << c2.size() << endl;
    cout << "stack.top()= " << c2.top() << endl;
    cout << "use stack milli-seconds : " << (clock() - timeStart) << endl;
}
int test_Initialization_list() {
    clock_t timeStart = clock();
    float* tempArr2 = new float[10];
    memset(tempArr2,0,10*sizeof(float));
    for (int i = 0; i < 11; i++)
    {
        cout << "use stack milli-seconds : " << tempArr2[i] << endl;
    }
    
}


int main(int argc, char** argv){
    test_Initialization_list();
    // test_priority_queue();
    // char ch;
    // char Name[20],Id[20];
    // int Math,Chinese,English;
    // fstream ioFile;
    // ioFile.open("./per.dat",ios::out|ios::app);
    // cout<<"---------建立学生档案信息----------\n";
    // do{
    //     cout<<"请输入姓名：";
    //     cin>>Name;
    //     cout<<"请输入身份证号：";
    //     cin>>Id;
    //     cout<<"请输入数学成绩：";
    //     cin>>Math;
    //     cout<<"请输入汉语成绩：";
    //     cin>>Chinese;
    //     cout<<"请输入英语成绩：";
    //     cin>>English;
    //     Person per(Name,Id,Math,Chinese,English);
    //     ioFile.write((char *)&per,sizeof(per));
    //     cout<<"您是否继续建档？(Y/y)  ";
    //     cin>>ch;
    // }while(ch=='y'||ch=='Y');
    // ioFile.close();
    // ioFile.open("./per.dat",ios::in);
    // Person p;
    // ioFile.read((char*)&p,sizeof(p));
    // vector<Person> v;
    // vector<Person>::iterator vt;
    // while(!ioFile.eof()){
    //     v.push_back(p);
    //     ioFile.read((char*)&p,sizeof(p));
    // }
    // ioFile.close();
    // cout<<"---------输出档案信息-----------"<<endl;
    // for(vt=v.begin();vt!=v.end();vt++){
    //     (*vt).display();
    // }
    // char s[20]="this is a string";
    // double digit=-36.96656;
    // cout<<setw(30)<<left<<setfill('*')<<s<<endl;
    // cout<<dec<<setprecision(4)<<digit<<endl;
    // cout<<dec<<15<<endl;
    // //setbase(int x)设置进制后，后面所有操作都是按照这个进制来计算！
    // cout<<setbase(10)<<15<<endl;
    // //四舍五入,并保留2位有效数组
    // float x=6.6937;
    // cout<<float(int(x*1000+0.5)/1000.0)<<endl;
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}