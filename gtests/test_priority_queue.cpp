#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;

struct priority_queue
{
    int* data;
    int cnt, size;
};

priority_queue* init(int n){
    priority_queue* q = new priority_queue();
    q->data = new int[n];
    q->cnt = 0;
    q->size = n;
    return q;
}

void clear(priority_queue* q){
    if (q == nullptr) return;
    delete q->data;
    delete q;
    return;
}
int empty(priority_queue* q){
    return q->cnt == 0;
}
int top(priority_queue* q){
    return q->data[1];
}
// void swap(int a, int b){ //值传递不行 用宏定义是在作用域内替换代码
//     int t = a;
//     a = b;
//     b = t;
// }
#define swap(a, b){\
    __typeof(a) __tmp = a;\
    a = b;\
    b = __tmp;\
}
int push(priority_queue* q, int val){
    if(q == nullptr) return 0;
    if(q->cnt == q->size) return 0;
    q->data[++(q->cnt)] = val;
    //向上调整
    int ind = q->cnt;
    while (ind >> 1 && q->data[ind] > q->data[ind >> 1])
    {
        swap(q->data[ind], q->data[ind >> 1]);
        ind >>= 1;
    }
    return 1;
}

int pop(priority_queue* q)
{
    if(q == nullptr) return 0;
    if(empty(q)) return 0;
    q->data[1] = q->data[q->cnt--];
    int ind = 1;
    while ((ind << 1) <= q->cnt)
    {
        int tmp = ind, l = ind << 1, r = (ind << 1) + 1;
        if (q->data[l] > q->data[tmp]) tmp = l;
        if (r <= q->cnt && q->data[r] > q->data[tmp]) tmp = r;
        if (tmp == ind) break;
        swap(q->data[ind],q->data[tmp]);
        ind = tmp;
    }
    return 1;
}

int main(int argc, char** argv){
    srand(time(0));
    #define MAX_OP 20
    priority_queue* q = init(MAX_OP);
    for (size_t i = 0; i < MAX_OP; i++)
    {
        int val = rand() % 100;
        push(q,val);
        cout << "insert " << val << " to the queue " <<endl;
    }
    for (size_t i = 0; i < MAX_OP; i++)
    {
        cout << top(q) << " ";
        pop(q);
    }
    cout << endl;
    clear(q);
    return 0;
}