#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
using namespace std;
  int dis_min = 99999999, book[101] = {0}, n, e[101][101] = {0};

void DisDFS(int cur, int dis){
  if(dis > dis_min) return;
  if (cur == n)
  {
    if(dis < dis_min) dis_min = dis;
    return;
  }
  for (size_t i = 1; i <= n; i++)
  {
    if (e[cur][i] != 99999999 && book[i] == 0)
    {
      book[i] = 1;
      DisDFS(i,dis+e[cur][i]);
      book[i] = 0;
    }
    
  }
  return;
}
void test_minDis(){
  int m,a,b,c;
  cin >> n >> m;
  for (size_t i = 1; i <= n; i++)
  {
    for (size_t j = 1; j <= n; j++)
    {
      if(i==j) e[i][j] = 0;
      else e[i][j] = 99999999;
    }
  }
    for (size_t d = 1; d <= m; d++)
    {
      cin >> a >> b >> c ;
      e[a][b] = c;
    }
    book[1] = 1;
    DisDFS(1,0);
    cout << "最短距离：" << dis_min << endl;
    
    return;
  
}

int main(int argc, char** argv){
  test_minDis();
  return 0;

}