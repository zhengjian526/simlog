# define NDEBUG // 忽略断言
#include "gtest/gtest.h"
#include "test.h"
using namespace std;

/**
 * @brief 获取vptr地址与func地址,vptr指向的是一块内存，这块内存存放的是虚函数地址，这块内存就是我们所说的虚表
 *
 * @param obj
 * @param offset
 *
 * @return 
 */
Fun getAddr(void* obj,unsigned int offset)
{
    cout<<"======================="<<endl;
    void* vptr_addr = (void *)*(unsigned long *)obj;  //64位操作系统，占8字节，通过*(unsigned long *)obj取出前8字节，即vptr指针
    printf("vptr_addr:%p\n",vptr_addr);

    /**
     * @brief 通过vptr指针访问virtual table，因为虚表中每个元素(虚函数指针)在64位编译器下是8个字节，因此通过*(unsigned long *)vptr_addr取出前8字节，
     * 后面加上偏移量就是每个函数的地址！
     */
    void* func_addr = (void *)*((unsigned long *)vptr_addr+offset);
    printf("func_addr:%p\n",func_addr);
    return (Fun)func_addr;
}
TEST(TestLog, case_virtual_func)
{
    
    Base ptr;
    Derived d;
    Base *pt = new Derived(); // 基类指针指向派生类实例
    Base &pp = ptr; // 基类引用指向基类实例
    Base &p = d; // 基类引用指向派生类实
    cout<<"基类对象直接调用"<<endl;
    ptr.fun1();
    cout<<"基类对象调用基类实例"<<endl;
    pp.fun1(); 
    cout<<"基类指针指向派生类实例并调用虚函数"<<endl;
    pt->fun1();
    cout<<"基类引用指向派生类实例并调用虚函数"<<endl;
    p.fun1();
    cout << "------" <<endl;
    // p.fun3();

    d.fun1();
    d.fun1(3);
    d.fun2();
    d.fun3();

    // 手动查找vptr 和 vtable
    Fun f1 = getAddr(pt, 0);
    (*f1)();
    Fun f2 = getAddr(pt, 1);
    (*f2)();
    delete pt;
    cout << "111-----" << endl;
}
TEST(TestLog, default_arg)
{
    Derived d;
    Base *pt = &d; // 基类指针指向派生类实例
    pt->fun();

}

TEST(TestLog, virtual_private)
{
    Derived d;
    Base *pt = &d; // 基类指针指向派生类实例
    pt->fun_p();

}
TEST(TestLog, virtual_func_inline)
{
    Base b;
    b.who();
    Derived d;
    Base *pt = &d; // 基类指针指向派生类实例
    pt->who();

}
TEST(TestLog, virtual_func_rtti)
{
  class B { virtual void fun() {} }; 
  class D: public B { }; 
 B *b = new D;  // 向上转型
    B &obj = *b;
    D *d = dynamic_cast<D*>(b);   // 向下转型
    if(d != NULL) 
        cout << "works"<<endl; 
    else
        cout << "cannot cast B* to D*"; 
 
    try {
        D& dobj = dynamic_cast<D&>(obj);  
        cout << "works"<<endl; 
    } catch (bad_cast bc) { // ERROR
        cout<<bc.what()<<endl;
    }
}
TEST(TestLog, assert_test)
{
    int x=7;
    assert(x==5);
}

int main(int argc, char** argv){
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}