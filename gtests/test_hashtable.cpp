#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;

struct Node
{
    char* str;
    Node* next;
};
struct HashTable
{
    Node** data;
    int size;
};

Node* init_node(char* str, Node* head)
{
    Node* p = new Node;
    p->str = strdup(str);
    p->next = head;
    return p;
}
HashTable* init_hashtable(int n)
{
    HashTable* h = new HashTable;
    h->size = n << 1; //50%的利用率
    h->data = new Node*[sizeof(Node*) * (h->size)];
    return h;
}

int BKDRHash(char* str){
    int seed = 31;
    int hash = 0;
    for (size_t i = 0; str[i]; i++)
    {
        hash = hash * seed + str[i];
    }
    return hash & 0x7fffffff;
}

int insert(HashTable* h, char* str){
    int hash = BKDRHash(str);
    int ind = hash % h->size;
    h->data[ind] = init_node(str,h->data[ind]);

}
int search(HashTable* h, char* str)
{
    int hash = BKDRHash(str);
    int ind = hash % h->size;
    Node* p = h->data[ind];
    while (p && strcmp(p->str,str)) p = p->next;
    return p != nullptr;
}

void clear_node(Node* node){
    if(node == nullptr) return;
    Node* p = node, *q;
    while (p != nullptr)
    {
        q = p->next;
        delete p->str;
        delete p;
        p = q;
    }
    return;  
}
void clear(HashTable* h)
{
    if (h == nullptr) return;
    for (size_t i = 0; i < h->size; i++)
    {
        clear_node(h->data[i]);
    }
    delete h->data;
    delete h;
    return;
}

int main(int argc, char** argv){
    int op;
    #define MAX_N 100
    char str[MAX_N + 5];
    HashTable* h = init_hashtable(MAX_N + 5);
    while (cin >> op >> str)
    {
        switch (op)
        {
        case 0:
            cout << "insert " << str << " to hash table" << endl;
            insert(h, str);
            break;
        case 1:
            cout << "search " <<  str << " from hashtable result = " << search(h,str) << endl;
        default:
            break;
        }
    }
    #undef MAX_N
    clear(h);
    return 0;
}