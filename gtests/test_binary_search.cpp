#include "gtest/gtest.h"
#include <limits.h>
#include <stdlib.h>
#include <memory>
#include <time.h>
using namespace std;

int binary_search(int* arr, int n, int x)
{
    int head = 0, tail = n - 1, mid ;
    while (head <= tail)
    {
        mid = (head + tail) >> 1;
        if(arr[mid] == x) return mid;
        if (arr[mid] < x) head = mid + 1;
        else tail = mid - 1;       
   }
   return -1;
}

//000000011111111 查找第一个1 
int binary_search1(int* arr, int n)
{
    int head = 0, tail = n; // 虚拟尾指针
    int mid;  
    while (head < tail)
    {
        mid = (head + tail) >> 1;
        if (arr[mid] == 1) tail = mid;
        else head = mid + 1;
    }
    return head == n ? -1: head;
}
//111111110000000 查找最后一个1 
int binary_search2(int* arr, int n)
{
    int head = -1, tail = n-1; // 虚拟尾指针
    int mid;  
    while (head < tail)
    {
        mid = (head + tail + 1) >> 1;
        if (arr[mid] == 1) head = mid;
        else tail = mid - 1;
    }
    return head;
}


int main(int argc, char** argv){
    int arr[100] = {0};
    int n;
    cin >> n ;
    for (size_t i = 0; i < n; i++)
    {
        cin >> arr[i];
    }
    // int x;
    // while (cin >> x)
    // {
    //     cout << "find it! pos: "<< binary_search(arr,n,x) << endl;
    // }
    // cout << "find it! pos: "<< binary_search1(arr,n) << endl;
    cout << "find it! pos: "<< binary_search2(arr,n) << endl;
    return 0;

}