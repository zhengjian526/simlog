/*
* Filename: main.cpp
* Author: L.Y.
* Brief: SHA256算法测试
*/

#include <iostream>
#include <string>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>

#include "sha256.h"
using namespace std;

int main()
{
    std::string message = ""; // 待加密的信息
    string file_name = "./jiamiceshi.txt";
    int len = 5652480;
    void* addr = new char[len]; 
    ifstream fin(file_name.c_str(),ios_base::in | ios_base::binary);
    if (fin.is_open())
    {
        while (fin.read((char*)addr,len))
        {
            cout<<  "read successfully";
        }
        fin.close();
    }


    ly::Sha256 sha256; // 初始化Sha256对象
    clock_t start = clock();

    std::string message_digest = sha256.getHexMessageDigest((char*)addr,len); // 加密
    // std::string message_digest = sha256.getHexMessageDigest(message); // 加密
    clock_t end = clock();
    double dur = (double)(end - start);
    std::cout << "加解密时间：" << dur/CLOCKS_PER_SEC << "s"<< std::endl;

    std::cout << message_digest << std::endl; // 输出加密结果
    getchar();
    
	return 0;
}