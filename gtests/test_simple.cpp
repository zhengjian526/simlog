#include "gtest/gtest.h"
#include "test.h"
using namespace std;

struct stuff 
{
    unsigned int field1: 30;
    unsigned int       : 2;
    unsigned int field2: 4;
    unsigned int       : 0;
    unsigned int field3: 3; 
};
struct box {
    unsigned int ready:     2;
    unsigned int error:     2;
    unsigned int command:   4;
    unsigned int sector_no: 24;
}b1;
union u_box {
  struct box st_box;     
  unsigned int ui_box;
};

TEST(TestLog, bit_field)
{
    struct stuff s={1,3,5};
    cout<<s.field1<<endl;
    cout<<s.field2<<endl;
    cout<<s.field3<<endl;
    cout<<sizeof(unsigned int)<<endl;
    cout<<sizeof(s)<<endl;
}
TEST(TestLog, union_bit_field)
{
    union u_box u;
    u.ui_box = 0;
    cout<<u.ui_box<<endl;
    cout<<u.st_box.command<<endl;
    cout<<u.st_box.error<<endl;
    cout<<sizeof(u.st_box)<<endl;
}

struct BS {         
    int v1;
   private:   //error!
        int v3;
    public:     //显示声明public
        int v2;
    void print(){       
        printf("%s\n","hello world");
    };    
};

TEST(TestLog, test_struct)
{
    struct BS base1;  //ok
    BS base2; //ok
    BS base;
    base.v1=1;
    // base.v3=2;
    base.print();
    printf("%d\n",base.v1);
    // printf("%d\n",base.v3);
}

class FriendFunc
{
public:
    FriendFunc(int _a):a(_a){};
    friend int geta(FriendFunc &ca);  ///< 友元函数
private:
    int a;
};

int geta(FriendFunc &ca) 
{
    return ca.a;
}
TEST(TestLog, test_friend_func)
{
    FriendFunc a(3);    
    cout<<geta(a)<<endl;
}
int f(int n)
{
    if(n==0)
    {
        return 1;
    }
    else
    {
        return n*f(n-1);
    }
}
TEST(TestLog, test_recursive1)
{
    cout<<"input x:";
    int x;
    cin>>x;
    cout<<f(x)<<endl;
}
void move(char A, char B)
{
     cout<<A<<"->"<<B<<endl;
}

void hanoi(int n, char A, char B, char C)
{
    cout << (n=1) << endl;
    if (n == 1)
    {
        move(A,C);
    }
    else
    {
        hanoi(n-1,A,C,B);
        move(A,C);
        hanoi(n-1,B,A,C);
    }
    
}
TEST(TestLog, test_recursive2)
{
    cout<<"请输入盘子数量：";
    int disks;
    cin>>disks;
    hanoi(disks,'A','B','C');
}
struct student
{
    int num;
    char name[20];
    char gender;
};
TEST(TestLog, test_struct2)
{
    student s={10,"asd",'M'};
    cout<<s.num<<endl;
    cout<<sizeof(s.num)<<endl;
    cout<<sizeof(s.name)<<endl;
    cout<<sizeof(s.gender)<<endl;
    cout<<sizeof(s)<<endl;
}
#define exp(s) printf("test s is:%s\n",s)
#define exp1(s) printf("test s is:%s\n",#s)
#define exp2(s) #s 

#define expA(s) printf("前缀加上后的字符串为:%s\n",gc_##s)  //gc_s必须存在
// 注意事项2
#define expB(s) printf("前缀加上后的字符串为:%s\n",gc_  ##  s)  //gc_s必须存在
#define gc_hello1 "I am gc_hello1"
union myun 
{
    struct { int x; int y; int z; }u; 
    int k; 
}a; 
int main(int argc, char** argv){

    // a.u.x =4;
    // a.u.y =5; 
    // a.u.z =6; 
    // a.k = 22; //覆盖掉第一个int空间值
    // printf("%d %d %d %d\n",a.u.x,a.u.y,a.u.z,a.k);
    // return 0;
    // const char * gc_hello = "I am gc_hello";
    // expA(hello);
    // expB(hello1);
    // return 0;
    // exp("hello");
    // exp1(hello);

    // string str = exp2(   bac );
    // cout<<str<<" "<<str.size()<<endl;
    // /**
    //  * 忽略传入参数名前面和后面的空格。
    //  */
    // string str1 = exp2( asda  bac );
    // /**
    //  * 当传入参数名间存在空格时，编译器将会自动连接各个子字符串，
    //  * 用每个子字符串之间以一个空格连接，忽略剩余空格。
    //  */
    // cout<<str1<<" "<<str1.size()<<endl;
    // return 0;
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}