#include "gtest/gtest.h"
#include <math.h>
#include<iostream>
#include<fstream>
#include <queue>
#include<vector>
#include "object.h"
#include "type.h"
using namespace std;


class FloatObj : public Object {
 public:
  FloatObj();
  DataType dtype;
  double value;
  static constexpr const char *_type_key = "Float";
  RB_DECLACE_OBJECT_INFO(FloatObj, Object);
};

class Float : public ObjectRef {
 public:
  Float() : Float(0.) {}
  Float(double value, DataType dtype = DataType::Float(32));

  template <typename T, typename = typename std::enable_if<
                            std::is_floating_point<T>::value>::type>
  operator T() const {
    auto res = (*this)->value;
    CHECK_GT(std::numeric_limits<T>::max(), res);
    CHECK_GT(res, std::numeric_limits<T>::min());
    return T(res);
  }

  RB_DEFINE_NOTNULLABLE_OBJECT_REF_METHODS(Float, ObjectRef, FloatObj)
};

Float::Float(double value, DataType dtype) {
  auto n = make_object<FloatObj>();
  n->value = value;
  n->dtype = dtype;
  ptr = n;
}
TEST(TestLog, test_make_obj)
{

}


int main(int argc, char** argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}