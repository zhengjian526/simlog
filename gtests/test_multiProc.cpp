#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#define BEGINS(x) namespace x { //namespace x
#define ENDS(x) }
BEGINS(test1)
int main(){
    for (size_t i = 0; i < 10; i++)
    {
        pid_t pid = fork();
        if (pid < 0)
        {
            perror("fork");
            exit(1);
        }
        if(pid == 0){
            sleep(1);
            printf("child[%d], self=%d, parent=%d\n",i,getpid(),getppid());
            break;
        }
        
    }
    return 0;
}
ENDS(test1)

BEGINS(test2)
int main(int argc, char const *argv[])
{
}
ENDS(test2)

BEGINS(test3)
int main(){
}
ENDS(test3)
int main(int argc, char const *argv[])
{
    test1::main();
    // test2::main();
    return 0;
}
