#include "cereal/types/memory.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/base_class.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/types/polymorphic.hpp"
#include "cereal/access.hpp"
#include <vector>
#include <fstream>
using namespace std;

enum class Type
{
    BIRD = 1,
    DUCK,
    NONE
};

class IFly
{
public:
    IFly(const Type t) :type_(t) {}

public:
    virtual void fly() = 0;
    virtual IFly& get() = 0;

private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar)
    {
        ar(type_);
    }

    template<class Archive>
    static void load_and_construct(Archive & ar, cereal::construct<IFly> & construct)
    {
        Type t;
        ar(t);
        construct(t);
    }

protected:
    Type type_;
};

class Duck : public IFly
{
public:
    Duck(uint32_t id) : IFly(Type::DUCK), id_(id) 
    {
        shared_ptr<void> sp(new char[2], [](char *p){delete[] p;}); 
        void* t_ptr = sp.get();
        ((char*)t_ptr)[0] = 1;
        ((char*)t_ptr)[1] = 3;
        dataAddr_ = sp;
    }
    void fly() { cout << "duck " << id_ << " is flying." << endl; }
    uint32_t aa=33 ;
    std::shared_ptr<void> dataAddr_;
    Duck& get()
    {
        Duck* p = new Duck(11);
        p->aa = 33;
        return *p;
    }
    
private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar)
    {
        // 这里序列化了IFly基类
        ar(id_, cereal::base_class<IFly>(this));
    }

    template<class Archive>
    static void load_and_construct(Archive & ar, cereal::construct<Duck> & construct)
    {
        uint32_t id;
        ar(id);
        construct(id);
        // 对基类IFly进行反序列化
        ar(cereal::base_class<IFly>(construct.ptr()));
    }

protected:
    uint32_t id_;
};

// BlackDuck继承自Duck类
class BlackDuck : public Duck
{
public:
    BlackDuck(uint32_t id) : Duck(id) {}
    void fly() { cout << "black duck " << id_ << " is flying." << endl; }
    BlackDuck& get()
    {
        BlackDuck* p = new BlackDuck(12);
        return *p;
    }
    template<class Archive>
    void serialize(Archive &ar)
    {
        // BlackDuck没有成员变量，因此直接对父类进行序列化
        ar(cereal::base_class<Duck>(this));
    }

    template<class Archive>
    static void load_and_construct(Archive & ar, cereal::construct<BlackDuck> & construct)
    {
        // 随便填个值进行构造
        uint32_t id = 0;
        construct(id);
        // 对基类进行反序列化
        ar(cereal::base_class<Duck>(construct.ptr()));
    }
};

// 该类为模板类
template <typename T>
class Bird :public IFly
{
public:
    // 该类的构造为私有函数，通过EnableMakeShared类继承Bird类的方式以便于使用make_shared方法构造对象，该方法的参考链接见下方的说明
    // 这个类本应该是create方法的内部类，但那样的话就没法使用CEREAL_REGISTER_TYPE宏注册该类了
    // 为了序列化，无奈将该类改为了Bird类的内部类，且属性为public
    struct EnableMakeShared :public Bird<T>
    {
        template <typename...Args>
        EnableMakeShared(Args&&... args) :Bird(std::forward<Args>(args)...) {}

    private:
        // 由于创建的实际是EnableMakeShared实例，因此该实例也要实现正反序列化函数
        friend class cereal::access;
        template<class Archive>
        void serialize(Archive &ar)
        {
            ar(cereal::base_class<Bird<T>>(this));
        }

        template<class Archive>
        static void load_and_construct(Archive & ar, cereal::construct<EnableMakeShared> & construct)
        {
            uint32_t id;
            T data;
            ar(id, data);
            construct(id, data);
        }
    };

    // 静态方法，用于使用make_shared来创建私有构造的Bird类
    template <typename...Args>
    static std::shared_ptr<Bird<T>> create(Args&&... args)
    {
        return static_pointer_cast<Bird<T>>(make_shared<EnableMakeShared>(std::forward<Args>(args)...));
    }

public:
    void fly() { cout << "bird " << id_ << " is flying and data is " << data_ << endl; }
    Bird<T>& get()
    {
        Bird<T>* p = new Bird<T>(12,data_);
        return *p;
    }

private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar)
    {
        // 由于子类有确定的类型，因此实际上没有必要对父类的type进行序列化
        ar(id_, data_);
    }

    template<class Archive>
    static void load_and_construct(Archive & ar, cereal::construct<Bird<T>> & construct)
    {
        // 同序列化，无需对父类的type进行反序列化
        uint32_t id;
        T data;
        ar(id, data);
        construct(id, data);
    }

private:
    // 构造函数是私有的
    Bird(uint32_t id, T data) :IFly(Type::BIRD), id_(id), data_(data) {}

private:
    uint32_t id_;
    T data_;
};

// 使用CEREAL_REGISTER_TYPE注册子类
CEREAL_REGISTER_TYPE(Duck);
CEREAL_REGISTER_TYPE(BlackDuck);
// 因为create方法实际上创建的是EnableMakeShared类，所以这里也需要注册EnableMakeShared类
// 由于Bird类是模板类，因此需要对不同的模板参数分别进行注册
CEREAL_REGISTER_TYPE(Bird<uint32_t>::EnableMakeShared);
CEREAL_REGISTER_TYPE(Bird<std::string>::EnableMakeShared);

// CEREAL_REGISTER_POLYMORPHIC_RELATION宏用于cereal在不清楚继承链路时指明继承链路
// 如果子类中有使用cereal::base_class对父类进行正反序列化，那么cereal便清楚继承链路
// 但若没有使用到cereal::base_class，就需要使用CEREAL_REGISTER_POLYMORPHIC_RELATION宏指明
// 在EnableMakeShared类中虽然使用了cereal::base_class，但这仅指明了EnableMakeShared到Bird类的继承
// 而在Bird类中并没有使用cereal::base_class来指明Bird类到IFly类的继承
// 所以这里还是需要指明Bird类到IFly类的继承关系
CEREAL_REGISTER_POLYMORPHIC_RELATION(IFly, Bird<uint32_t>);
CEREAL_REGISTER_POLYMORPHIC_RELATION(IFly, Bird<std::string>);
// 实际上将上述两条语句改为指明IFly与EnableMakeShared类之间的关系也是可以的
//CEREAL_REGISTER_POLYMORPHIC_RELATION(IFly, Bird<uint32_t>::EnableMakeShared);
//CEREAL_REGISTER_POLYMORPHIC_RELATION(IFly, Bird<std::string>::EnableMakeShared);

int main(int argc, char** argv){
    {
        auto v = make_unique<vector<shared_ptr<IFly>>>();
        v->push_back(make_shared<Duck>(1));
        v->push_back(Bird<uint32_t>::create(2, 10000));
        v->push_back(Bird<std::string>::create(3, "string data"));
        v->push_back(make_shared<BlackDuck>(4));
        v->push_back(make_shared<BlackDuck>(5));
       
        for (auto &itr : *v) { itr->fly(); }
        cout << "before: " << (static_cast<Duck*>((*v).at(0).get()))->aa << endl;
        cout << "before: " << (int)((char*)((static_cast<Duck*>((*v).at(0).get()))->dataAddr_.get()))[0] << endl;
        cout << "before: " << (int)((char*)((static_cast<Duck*>((*v).at(0).get()))->dataAddr_.get()))[1] << endl;
        // 这里生成二进制数据
        std::ofstream os("out.cereal", std::ios::binary);
        cereal::BinaryOutputArchive archive(os);
        archive(v);
    }
    printf("================================\n");
    {
        auto v = make_unique<vector<shared_ptr<IFly>>>();
        
        // 对刚刚序列化生成的out.cereal文件进行反序列化
        std::ifstream os("out.cereal", std::ios::binary);
        cereal::BinaryInputArchive archive(os);
        archive(v);

        for (auto &itr : *v) { itr->fly(); }
        cout << "after: " << (static_cast<Duck*>((*v).at(0).get()))->aa << endl;
        cout << "before: " << (int)((char*)((static_cast<Duck*>((*v).at(0).get()))->dataAddr_.get()))[0] << endl;
        cout << "before: " << (int)((char*)((static_cast<Duck*>((*v).at(0).get()))->dataAddr_.get()))[1] << endl;
    }

    getchar();
    return 0;

}