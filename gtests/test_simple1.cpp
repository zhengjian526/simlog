#include "gtest/gtest.h"
#include "test.h"
using namespace std;
int i=1;  // i 为全局变量，具有静态生存期。
void other(void)
{
    static int a=2;
    static int b;
    // a,b为静态局部变量，具有全局寿命，局部可见。
    //只第一次进入函数时被初始化。
    int c=10;   // C为局部变量，具有动态生存期
    //每次进入函数时都初始化。
    a=a+2; i=i+32; c=c+5;
    cout<<"---OTHER---\n";
    cout<<" i: "<<i<<" a: "<<a<<" b: "<<b<<" c: "<<c<<endl;
    b=a;
}

TEST(TestLog, test_static_val)
{
    static int a;  // 静态局部变量，有全局寿命，局部可见。
    int b=-10;  // b, c为局部变量，具有动态生存期。
    int c=0;
    void other(void);
    cout<<"---MAIN---\n";
    cout<<" i: "<<i<<" a: "<<a<<" b: "<<b<<" c: "<<c<<endl;//1 0 -10 0
    c=c+8;  other();// 33 4 0 15
    cout<<"---MAIN---\n";
    cout<<" i: "<<i<<" a: "<<a<<" b: "<<b<<" c: "<<c<<endl;//33 0 -10 8 
    i=i+10; other();  //75 6 4 15
    other(); //107 8 6 15
}

enum weekday
{
 s,m,t,w,thu,f,s1
};

TEST(TestLog, test_enum)
{
    enum weekday wek=s;
    // weekday wek=s;
    for(int i=wek;i!=f;i++)
    {
        cout<<i<<endl;
        cout<<wek+s<<endl;
        cout<<"-------哈哈-------"<<endl;
    }
}


// double power(double x, int n)
// {
//     double val = 1.0;
//     while(n--)
//     {
//         val*=x;
//     }
//     return val;
// }
// TEST(TestLog, test_random)
// {
//         int x;
//     cin>>x;
//     int wei=0;
//     int sum=0;
//     int each,chu;
//     for(int i=0;i<8;i++)
//     {
//         each=x%10;
//         chu=x/10;
//         sum+=each*power(2,wei);
//         x=chu;
//         wei++;
//     }
//     cout<<sum<<endl;
// }

// class Time{
// private:
//     int hh,mm,ss;
// public:
//     Time(int h=0,int m=0,int s=0):hh(h),mm(m),ss(s){}
//     void operator()(int h,int m,int s) {
//         hh=h;
//         mm=m;
//         ss=s;
//     }
//     void ShowTime(){
//         cout<<hh<<":"<<mm<<":"<<ss<<endl;
//     }
// };


class Time{
    private:
    int hh,mm,ss;
    public:
    Time(int h,int m,int s):hh(h),mm(m),ss(s){

    }
    void operator()(int h,int m,int s){
        hh = h;
        mm = m;
        ss = s;
    } 
    void ShowTime(){
        cout<<hh<<":"<<mm<<":"<<ss<<endl;
    }
};
TEST(TestLog, test_oper)
{
    Time t1(12,10,11);
    t1.ShowTime();
    t1.operator()(23,20,34);	
    t1.ShowTime();
    t1(10,10,10);               	
    t1.ShowTime();
}
void fun(int x){
    try{
        if (x==0)
            throw "异常";
    }catch(...){
        cout<<"in fun"<<endl;
        throw 1;
    }
}

class Stack
{
private:
    int top;
    int elems[100];
public:
    Stack();
    ~Stack();
    void init() {
        top = -1;
    }
    bool IsFull()
    {
        if (top >= 99)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool IsEmpty(){
        if (top == -1)
        {
            return true;
        }
        else{
            return false;
        }
    }
    void push(int e){
        if(!IsFull())
        {
            elems[++top] = e;
        }
    }
    int pop(){
        if (!IsEmpty())
        {
            return elems[top--];
        }
        else{
        cout<<"栈已空，请不要再弹出元素！";
        return 0;
        }
    }
};

Stack::Stack(/* args */)
{
}

Stack::~Stack()
{
}

#define MAXSIZE 100

template<class T>
class Array{
    public:
        Array(){
            for(int i=0;i<MAXSIZE;i++){
                array[i]=0;
            }
        }

        T &operator[](int i);
        void Sort();


    private:
        T array[MAXSIZE];
};

template<class T> T& Array<T>::operator[](int i){
    if(i<0||i>MAXSIZE-1){
        cout<<"数组下标越界！"<<endl;
        exit(0);
    }
    return array[i];
}

template<class T> void Array<T>::Sort(){
    int p,j;
    for(int i=0;i<MAXSIZE-1;i++){
        p=i;
        for(j=i+1;j<MAXSIZE;j++){
            if(array[p]<array[j])
                p=j;
        }
        T t;
        t=array[i];
        array[i]=array[p];
        array[p]=t;
    }   
}
template<> void Array<char *>::Sort(){
    int p,j;
    for(int i=0;i<MAXSIZE-1;i++){
        p=i;
        for(j=i+1;j<MAXSIZE;j++){
            if(strcmp(array[p],array[j])<0)
                p=j;
        }
        char* t=array[i];
        array[i]=array[p];
        array[p]=t;
    }
}

int main(int argc, char** argv){
    Array<int> a1;
    Array<char*>b1;
    a1[0]=1;a1[1]=23;a1[2]=6;
    a1[3]=3; a1[4]=9; 	
    a1.Sort();
    for(int i=0;i<5;i++) 
    cout<<a1[i]<<"\t";
    cout<<endl;	
    b1[0]="x1";	b1[1]="ya";	b1[2]="ad";
    b1[3]="be";	b1[4]="bc";	
    b1.Sort();
    for(int i=0;i<5;i++)
    cout<<b1[i]<<"\t";
    cout<<endl;	

  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}