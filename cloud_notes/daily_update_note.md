## 知识点
#### 1.++i 和 i++区别

作者：叶王
链接：https://www.zhihu.com/question/19811087/answer/80210083
来源：知乎

i++ 与 ++i 的主要区别有两个：
**1、 i++ 返回原来的值，++i 返回加1后的值。**
**2、 i++ 不能作为左值，而++i 可以。**

毫无疑问大家都知道第一点（不清楚的看下下面的实现代码就了然了），我们重点说下第二点。
**首先解释下什么是左值**（以下两段引用自中文维基百科『右值引用』词条）。

> 左值是对应内存中有确定存储地址的对象的表达式的值，而右值是所有不是左值的表达式的值。

一般来说，**左值是可以放到赋值符号左边的变量**。但

> 能否被赋值不是区分左值与右值的依据。比如，C++的const左值是不可赋值的；而作为临时对象的右值可能允许被赋值。**左值与右值的根本区别在于是否允许取地址&运算符获得对应的内存地址。**

比如，

```cpp
int i = 0;
int *p1 = &(++i); //正确
int *p2 = &(i++); //错误

++i = 1; //正确
i++ = 5; //错误
```


那么为什么『i++ 不能作为左值，而++i 可以』？
看它们各自的实现就一目了然了：
以下代码来自博客：[为什么(i++)不能做左值，而(++i)可以](https://link.zhihu.com/?target=http%3A//blog.csdn.net/zlhy_/article/details/8349300)

```cpp
// 前缀形式：
int& int::operator++() //这里返回的是一个引用形式，就是说函数返回值也可以作为一个左值使用
{//函数本身无参，意味着是在自身空间内增加1的
  *this += 1;  // 增加
  return *this;  // 取回值
}

//后缀形式:
const int int::operator++(int) //函数返回值是一个非左值型的，与前缀形式的差别所在。
{//函数带参，说明有另外的空间开辟
  int oldValue = *this;  // 取回值
  ++(*this);  // 增加
  return oldValue;  // 返回被取回的值
}
```

如上所示，i++ 最后返回的是一个临时变量，而**临时变量是右值**。

#### 2.通过大括号进行初始化的条件

![img](../AppData/Local/Temp/%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_16384400117273.png)

## 设计模式

#### 创建类



#### 结构类

##### 1.适配器模式

适配器类 继承于某个类，然后对这个类进行接口改造，使之能适配另一个类的对其调用；装饰模式在不改变对象接口的前提下强化对象的功能；与适配器模式提供不同的接口以适配调用不同，代理模式则是提供相同的接口，代替宿主完成某些功能。

##### 2.代理模式



##### 3.装饰模式

代码示例：https://refactoringguru.cn/design-patterns/decorator/cpp/example

 **装饰模式适合应用场景**
 如果你希望在无需修改代码的情况下即可使用对象， 且希望在运行时为对象新增额外的行为， 可以使用装饰模式。

 装饰能将业务逻辑组织为层次结构， 你可为各层创建一个装饰， 在运行时将各种不同逻辑组合成对象。 由于这些对象都遵循通用接口， 客户端代码能以相同的方式使用这些对象。

 **如果用继承来扩展对象行为的方案难以实现或者根本不可行， 你可以使用该模式。**

 许多编程语言使用 final最终关键字来限制对某个类的进一步扩展。 复用最终类已有行为的唯一方法是使用装饰模式： 用封装器对其进行封装。

总结：

- [适配器模式](https://refactoringguru.cn/design-patterns/adapter)可以对已有对象的接口进行修改， [装饰模式](https://refactoringguru.cn/design-patterns/decorator)则能在不改变对象接口的前提下强化对象功能。 此外， *装饰*还支持递归组合， *适配器*则无法实现。
- [适配器](https://refactoringguru.cn/design-patterns/adapter)能为被封装对象提供不同的接口， [代理模式](https://refactoringguru.cn/design-patterns/proxy)能为对象提供相同的接口， [装饰](https://refactoringguru.cn/design-patterns/decorator)则能为对象提供加强的接口。

#### 行为类

## Effective C++学习笔记

#### 条款3：尽量使用const

1.如果const关键字出现在星号左边，表示被指物是常量；如果出现在右边，则表示指针自身是常量；如果出现在星号两边，则表示被指物和指针两者都是常量。

2.const iterator作用类似于T* const指针，表示这个迭代器不得指向不同的东西，但是他所指的东西的值是可以改动的；如果希望迭代器指的东西不能改动，则使用const_iterator迭代器。

3.**const成员函数**

两个成员函数常量性不同，是可以被重载的。

#### 条款9：绝不在构造函数和析构过程中调用virtual函数

1.构造过程中基类总是先于派生类进行构造（析构则相反），由于base class构造函数的执行更早于derived class 构造函数，当baseclass 构造函数执行时derived class的成员变量尚未进行初始化。

2.在base class 构造过程中，virtual 函数不是virtual函数。

同样的道理适用于析构函数。

#### 条款10：令operator=返回一个reference to *this

![image-20210816113233078](daily_update_note.assets/image-20210816113233078.png)

#### 条款12：复制对象时勿忘其每个成分

写继承类的拷贝构造和拷贝赋值函数时，要小心基类的成员复制。

![image-20210816115617817](daily_update_note.assets/image-20210816115617817.png)

#### 条款20：宁以pass-by-reference-to-const 替换pass-by-value

1.当不需要改变传入参数时，尽量使用const T &来替代按值传递

2.使用by-reference-to-const的方式传递能有效避免继承类被切割的问题

![image-20210816182409154](daily_update_note.assets/image-20210816182409154.png)

3.内置类型值传递的效率优于按引用传递。

#### 条款23：宁以non-member non-friend函数替换member函数

1.使用同一个namespace下的函数可以便与对工程文件进行组织，不同的函数可以写入不同的头文件中，但是类不能分割。

![image-20210817201552660](daily_update_note.assets/image-20210817201552660.png)

优点：可以增加封装性，包裹性和机能扩展性

#### 条款25：写出一个不抛异常的swap函数

没看懂 建议学习后重看

#### 5 实现

#### 条款26：那可能延后变量定义式的出现时间

1.不只应该延后变量的定义，直到非得使用该变量的前一刻。更应该尝试延后这份定义直到能够给他初值实参为止。这样不仅能够避免构造和析构非必要的对象，还可以避免无意义的default构造行为。

#### 条款27：尽量少做转型动作



#### 条款31：将文件间的编译依赖关系降至最低

![image-20210825180138099](daily_update_note.assets/image-20210825180138099.png)

实际上是在接口类A中将成员变量单独拿出来设计一个类B，而接口类A中只留B的指针。

（1）如果使用object reference或者object pointer可以完成任务，就不要使用objects。

（2）尽量以class声明式替换class定义式；

（3）为声明式和定义式提供不同的头文件；

#### 继承与面向对象设计

#### 条款32：确定你的public继承模型塑造出is-a关系

public继承意味着is-a。适用于base classes身上的每一件事一定也适用于derived classes上。



#### 条款35：考虑虚函数以外的其他选择

经典策略模式（strategy）可采用function函数对象实现不同对象传入到函数中进行处理。

#### 条款39：明智而谨慎的使用private继承

1.class之间的private继承，并不意味着is-a，编译器不会自动将derived class对象转换成一个base class，这和public继承情况不同

2.有private base class继承而来的所有成员，在derived class中国都会变成private属性，即使他们在base class中原本是protected或者是public属性。

3.private继承意味着“根据某物实现出”，它通常比复合的级别低，但是当derived class需要访问protected base class的成员时，或需要重新定义继承而来的virtual函数时，这么设计是合理的；

4.和复合不同，private继承可以造成empty base class的最优化、这致力于“对象尺寸最小化”的程序开发者很重要。

#### 条款40：明智而谨慎的使用多重继承

![image-20210903164435355](daily_update_note.assets/image-20210903164435355.png)

1.菱形继承中 最底层的derived class 从每个 base class中继承了一份，如上图。如果File类中有fileName属性，那么IOFile 类中将有两个fileName同名属性。

解决这个问题可以采用**虚继承**

![image-20210903164618002](daily_update_note.assets/image-20210903164618002.png)

2.virtual继承会增加大小、速度、初始化复杂度等成本。如果virtual base classes不带任何数据，将是最具有实用价值的。

#### 7模板与泛型编程

#### 条款42：了解typename的双重意义

1.嵌套从属类型名称（nested dependent type name）想要被编译器解析为类型时，需要在紧邻它之前放置关键字typename。

2.使用关键字typename标识嵌套从属类型名称，但不得在base class lists（基类列）或者member initialization list（成员初始化列表）中以它作为base class修饰符。

![image-20210903175541248](daily_update_note.assets/image-20210903175541248.png)

#### 条款44：需要转换类型时请为模板定义非成员函数

1.在template实参推导过程中从不会将隐式类型转换纳入考虑。

2.当我们编写一个class template，而他所提供的“与此template相关的”函数支持“所有参数的隐式类型转换”时，

#### 条款47：请使用traits classes表现类型信息

模板编程重点理解。

编译器条件句的“if else”是通过函数重载实现的。

### --------------------公众号复制 effective C++ 学习笔记

#### 一、让自己习惯C++ 

### 条款01：视C++为一个语言联邦

C++并不是一个带有一组守则的一体语言：他是从四个次语言**( C、Object-Oriented C++、Template、STL )**  组成的联邦政府，每个次语言都有自己的规约。记住这四个次于语言你就会发现C++容易了解得多。

### 条款02:尽量以const,enum,inline替换 #define

```
#define ASPECT_RATIO 1.653
```

以上句为例，是通过预处理器处理而不是编译器处理，有可能 **ASPECT_RATIO** 没进入记号表内，于是如果出现了编译错误，那么编译器会提示错误信息是 **1.653** 而不是 **ASPECT_RATIO** ，你会感到非常困惑。

解决方法是用常量替换宏

```
const double AspectRatio = 1.653
```

这样编译器就可以看到**ASPECT_RATIO** ，而且使用常量会使代码量较小，因为预处理器只会盲目的替换而出现多份 **1.653**

- string对象通常比char* 更好一点

- 对于class的专属常量，为了限制**作用域在class内**,并且**防止产生**多个实体，最好使用**static**

- 

- 1. 如果你的编译器支持在类内对*const static 整数类型*声明时获初值，则使用
  2. 如果不支持，则在类内定义，在对应的实现文件中赋值

- 如果你需要在编译器就使用一个class常量值，则应最好改用**枚举类型**enum，且枚举不能用来取地址，不会为它分配**额外的存储空间**

- 对于形似函数的宏，最好改用**inline的模板函数**

### 条款 03:尽可能使用const

- const出现在星号**左边**目标是指**物是常量**,出现在星号右边表示**指针本身是常量**,如果出现在两边，则**指针和物都是常量**

- `void f1(const Widget* pw)`和`void f2(Widget const* pw)`两种写法意义相同,都表示被指物是常量

- 对于STL迭代器来说，如果你希望迭代器所指的动科不可改动，你需要的是const_iterator

- 令函数**返回**一个常量值，往往可以降低因客户错误而造成的意外(例如把一个值赋值给一个返回值)

- 将const实施与**成员函数**的目的是为了明确该成员函数可作用于const对象:

- 1. 他们使class接口比较容易理解
  2. 他们使得可以**操作const对象**

- const成员函数和no-const成员函数可重载，即可以同时出现，在传入不同的参数时候会调用不同的版本，但是有时我们需要这样，但是又不想代码重复，我们可以**在no-const成员调用const成员函数**来处理这个代码重复问题

- - 例如：`const_cast<char &>( static_cast<const TextBlock&>(*this)[position]);`,经过这样里面 先安全转型使得调用的是const版本，外面再去const转型

### 条款 04:确定对象被使用前已先被初始化

- 对于内置类型要进行**手工初始化**

- 构造函数最好**使用成员初值列表**，不要在构造函数中使用赋值操作来初始化，而且初值列表列出的成员变量次序应该和在class中声明的次序一样，因为**声明次序**就是C++保证的初始化次序

- 对于static对象，在跨编译单元之间的初始化次序是不能确定的，因为C++只保证在本文件内使用之前一定被初始化了

- - 举例(使用如下方式可以解决这个问题即**以loacl static对象替换non-local static对象**)：

```
class FileSystem{...};
FileSystem& tfs(){
    static FileSystem fs;
    return fs;
}
```

### 二、构造/析构/赋值运算

### 条款05:了解C++默默编写并调用了哪些函数

- 如果你不定义，编译器会自动帮你实习默认的*构造函数，析构函数，拷贝赋值运算符和拷贝构造函数*，但是如下几种情况不会替你生成默认的**拷贝赋值运算符**

- 1. 类中含有**引用**的成员变量
  2. 类中含有**const**的成员变量
  3. 类的**基类**中的拷贝赋值运算符是**私有**成员函数

### 条款06：若不想使用编译器自动生成的函数，就应该明确拒绝

- 当我们不希望编译器帮我们生成相应的成员函数的时候，我们可以将其声明为**private**并且**不予以实现**

### 条款07：为多态基类声明virtual析构函数

- 以下情况应该为类声明一个**virtual析构函数**:

- 1. 用来作为带有多态性质的**基类**的类
  2. 一个类中带有任何**virtual函数**

- 如果类的设计**目的**不是作为基类使用，那么就不应该为它声明virtual析构函数

### 条款08：别让异常逃离析构函数

- 析构函数不要吐出异常，如果实在要抛出异常，那么最好使用`std::abort();`，放在**catch**中，把这个行为压下去
- 如果某个动作可能会抛出异常，那么最好把它放在**普通**函数中，而不是放在析构函数里面，让客户来执行这个函数并去处理

### 条款09：绝不再构造和析构函数中调用virtual函数

- 在构造和析构的时候，不要试图调用或在调用的函数中调用virtual函数，因为会调用父类版本导致出现一些未定义的错误

> 解决办法之一：

```
class Transaction{
    publci:
     explicit Transaction(const std::string& logInfo);
     void logTransaction(const std::string& logIngo) const;//把它变成这样的non-virtual函数
     ...
};
Transaction::Transaction(const std::string& logInfo){
    ...
    logTransaction(logInfo);//这样调用
}
class BuyTransaction: public Transaction{
     BuyTransaction( parameters ):Transaction(createLogString( parameters )){...}//将log信息传给基类的构造函数
    private:
     static std::string createLogString( parameters );//注意此函数为static函数
}
```

### 条款10：令operator= 返回一个reference to *this

- 为了实现连锁赋值如内置类型`x= y = z =15`由于=采用**右结合律**，所以等价于`x = (y = (z = 15))`,因此，为了使我们自定义类也实现，所以**重载=,+=,-=,\*=**使其返回**refercence to \*this**

### 条款11：在operator= 中处理“自我赋值”

- 在赋值的时候会出现对自我进行赋值的情况，这种情况下我们很容易写出不安全的代码

```
Widget::operator=(const Widget& rhs){
 delete pb; //把自己释放了
 pb = new Bitmap(*rhs.pb);//这就不安全了
 return *this;
}
```

- 因此有三种**推荐**的做法

1. ##### 先验证是不是相同的，是不是自我赋值

```
Widget::operator=(const Widget& rhs){
if(this == &rhs) return *this;//验证是不是相同
 delete pb; 
 pb = new Bitmap(*rhs.pb);
 return *this;
}
```

1. ##### 在复制pb所指的东西之前别删除pb

```
Widget::operator=(const Widget& rhs){
 Bitmap* pOrig = pb;
 pb = new Bitmap(*rhs.pb);//让pb指向*pb的一个副本
    delete pOrig; //删除原先的pb
 return *this;
}
```

1. ##### 使用交换数据的函数

```
class Widget{
...
void swap(Widget& rhs);//交换*this和rhs的数据
...
};
Widget::operator=(const Widget& rhs){
 Widget temp(rhs);//创建一个rhs副本
 swap(temp);//交换*this和上面的副本
 return *this;
}
```

### 条款12：复制对象时勿忘其每一个成分

- 为了**确保**复制的时候复制对象内的所有成员变量，我们应该在字类的构造和赋值函数中**调用父类的**构造和赋值函数来完成各自的任务
- 不要尝试在复制构造函数和赋值函数中**相互调用**，如果想消除重复代码，请建立一个新的成员函数，并且最好将其设为**私有**且命名为**init**

### 三、资源管理

### 条款13：以对象管理资源

- 为了防止资源泄露，我们应该在**构造函数**中获取资源，在**析构函数**中释放资源，这样可以有效的避免资源泄露
- 使用智能指针是一个好的办法，在C++11中auto_ptr已经被弃用，有三个常用的是**unique_ptr,share_ptr和weak_ptr**

### 条款14：在资源管理类中心copying行为

- 我们在管理**RAII**(构造函数中获得，析构函数中释放)观念的类时，应该对不同的情况，根据不同的目的进行处理

- 1. 当我们处理不能同步拥有的资源的时候，可以才用**禁止复制**，如把copying操作声明为private
  2. 当我们希望共同拥有资源的时候，可以采用**引用计数法**，例如使用shared_ptr
  3. 当我们需要拷贝的时候，可以采用**深拷贝**
  4. 或者某些时候我们可以采用**转移**底部资源拥有权的方式

### 条款15：在资源管理类中提供对原始资源的访问

- 有的api函数往往需要访问类的原始资源，所以每一个RAII类应该提供一个返回其管理的原始资源的方法
- 返回原始资源可以使用显示转换也可以使用隐式转换，但是往往显示转换更加安全一点，但是隐式转换更加方便

```
class Font{
 ...
 FontHandle get() const {return f;} //显示转换
 ...
 operator FontHandle() const {return f;} //隐式转换函数
 ....
 private:
  FontHandle f; //管理的原始资源
}
```

### 条款16：成对使用new和delete时要采用相同形式

- 不要对**数组**形式做typedef,因为这样会导致delete的时候调用的是`delete ptr`而不是`delete [] ptr`,对内置类型会出现未定义或有害的，对类的类型会导致无法调用剩余的**析构函数**，导致类中管理的资源无法释放，从而造成内存泄漏
- 在new 表达式中使用[ ] ,则在相应的delete 表达式中也使用 [ ]

### 条款17：以独立语句将newed对象置入智能指针

- 诸如这样的语句  `processWidget (std::tr1::shared_ptr<Widget>(new Widget),priority())`

- - 因此要以**独立语句**将newd对象存储于智能指针中，把它分离出来。如果不这样，一旦异常被抛出，有可能导致难以察觉的资源泄漏

- 1. 在先执行  `new Widget`语句和调用  `std::tr1::shared_ptr`构造函数之间
  2. 不能确定`priority`函数的执行顺序，可能在最前面，也可能在他们的中间

### 四、设计与声明

### 条款18：让接口容易被正确使用，不易被误用

- 我们接口应该替客户着想，考虑周全，避免它们犯错误。例如在向函数传递日期的时候，把日期参数做成类的形式，并且用static成员函数来返回固定的月份，避免用户参数写错
- 接口应该和内置接口保持**一致**，避免让客户感觉不舒服，这方面STL做的很好
- tr1::shared_ptr支持**定制型删除器**，使用它可以防范跨DLL构建和删除的问题，可以用它来自动解除互斥锁

### 条款19：设计class犹如设计type

- 谨慎的设计一个类，应该遵守以下**规范**

- 1. 合理的构建class的构造函数、析构函数和内存分配函数以及释放函数
  2. 不能把初始化和赋值搞混了
  3. 如果你的类需要被用来以值传递，复制构造函数应该设计一个通过值传递的版本
  4. 你应该给你的成员变量加约束条件，保证他们是合法值，所以你的成员函数必须担负起错误检查工作
  5. 如果你是派生类，那么你应该遵守基类的一些规范，如析构函数是否为virtural
  6. 你是否允许你的class有转换函数，，是否允许隐式转换。如果你只允许explicit构造函数存在，就得写出专门负责执行转换的函数
  7. 想清楚你的类应该有哪些函数和成员
  8. 哪些应该设计为私有
  9. 哪个应该是你的friend,以及将他们嵌套与另一个是否合理
  10. 对效率，异常安全性以及资源运用提供了哪些保证
  11. 如果你定义的不是一个新type,而是定义整个type家族，那么你应该定义一个类模板
  12. 如果只是定义新的字类以便为已有的类添加机制，说不定单纯定义一个或多个non-member函数或模板更好

### 条款20：宁以pass-by-reference-to-const替换pass-by-value

- 尽量以pass-by-reference-to-const替换pass-by-value，因为前者通常比较**高效**，比如在含有类的传递时，避免了多次构造函数和多次析构函数的调用，大大的提高了效率
- 但是对于某些，比如*内置类型，迭代器，函数调用*等最好以值传递的形式

### 条款21：必须返回对象时，别妄想返回其reference

- 绝对不能返回指针或者一个引用指向一个**临时变量**，因为它存在**栈**中，一旦函数调用结束返回那么你得到的将是一个坏指针，也不能使用static变量来解决，你可以通过返回值 来解决

### 条款22：将成员变量声明为private

- **成员变量要声明为private**

- - protected并不比public更具封装性

- 1. 为了保证一致性
  2. 可以细微的划分访问和控制以及约束
  3. 内部更改后不影响使用

### 条款23：宁以non-member、non-friend、替换member函数

- 我们可以用non-member、non-friend函数来替换某些成员函数，可以增加类的封装性，包裹弹性和扩充性

### 条款24：若所有参数皆需要类型转换，请为此采用non-member函数

- 如果你需要为某个函数的所有参数（包括被this指针所指的那个隐喻参数）进行类型转换，那么这个函数必须是个non-member

### 条款25：考虑写出一个不抛出异常的swap函数

- 在你没有定义swap函数的情况下，编译器会为你调用通用的swap函数，但是有的时候那并不是**高效**的，因为默认情况它在置换如指针的时候把整个内存都置换

- - 我们采取一种解决办法

- 1. 在类中提供一个 public swap成员函数，并且这个函数**不能抛出异常**
  2. 在类的命名空间中提供一个**non-member swap函数**，并令它**调用**类中的swap函数
  3. 如果你正在编写一个类而不是模板类，为你的class特化std::swap函数，并令它调用你的swap函数
  4. 请在类中**声明** `using std::swap`,让其暴露，使得编译器**自行选择**更合适的版本

### 五、实现

### 条款26：尽可能延后变量定义式的出现时间

- 定义一个变量，那么你就得承受这个变量的**构造和析构**的成本时间，所以在定义一个变量的时候我们应该尽可能的**延后**定义时间，在使用前定义，这样避免我们定义了却没有使用它，造成浪费

### 条款27：尽量少做转型动作

- 旧式转型是C风格的转型，C++中提供四种**新式转型**：

- - 旧式转型使用的时机是，当要调用一个explicit构造函数对一个对象传递给一个函数时，其他尽量用新式转型

- 1. ##### const_cast 通常被用来将对象的常量性转除。它也是唯一有此能力的转型操作符

  2. ##### dynamic_cast 主要用来执行“安全向下转型” ，也就是用来决定对某对象是否归属继承体系中的某个类型。它是唯一无法由旧式语法执行的动作，也是唯一可能耗费重大运行成本的转型动作

  3. ##### reinterpret_cast 意图执行低级转型，实际动作（及结果）可能取决于编译器，这也就表示它不可移植。例如将一个pointer to int转型为一个int。这一类转型在低级代码以外很少见。

  4. ##### static_cast 用来强迫隐式转换，例如将non-const对象转换为const对象，或将int转为double等等，它也可以用来执行上述多种转换的反向转换，例如将void* 指针转为 type 指针，将pointer-to-base 转为 pointer-ro-derived 。但它无法将 const 转为 non-const ——这个只有const_cast才能办到

- 请记住以下：

- 1. 如果可以的话，避免dynamic_cast转型，如果实在需要，则可以试着用别的无转型方案**代替**
  2. 如果转型是必要的，那么应该把他**隐藏**于某个函数背后，客户随后可以调用该函数，而不是需要将转型放进自己的代码里
  3. 宁可要**新型**转型，也**不**要使用**旧式**转型

### 条款28：避免返回handles指向对象内部成分

- 避免返回handle（包括引用，指针和迭代器）指向对象内部。这样可以增加封装性，也能把出现**空悬指针**的可能性降低

### 条款29：为“异常安全”而努力是值得的

- 异常安全函数提供以下三个保证之一：

- - **基本承诺**：如果异常被抛出，程序内的任何事物仍然保持在有效状态下。没有任何对象或数据会因此而败坏，所有对象都处于一种内部前后一致的状态。然而程序的现实状态恐怕不可预料
  - **强烈保证**：如果异常被抛出，程序状态不改变。调用这样的函数需要有这样的认知：如果函数成功，就是完全成功，如果函数失败，程序会恢复到“调用之前”的状态
  - **不抛掷保证**：承诺绝不抛出异常，因为它们总是能够完成他们原先承诺的功能。作用于内置类型身上所有操作都提供nothrow保证，这是异常安全码中一个必不可少的关键基础材料

- 这三种保证是递增的关系，但是如果我们实在做不到，那么可以提供第一个基本承诺，我们在写的时候应该想如何让它具备异常安全性

- 1. 首先以对象管理资源可以阻止**资源泄漏**
  2. 在你能实现的情况下，尽量满足以上的最高等级

### 条款30：透彻了解inlining 的里里外外

- inline 声明的两种方式：

- 1. 隐喻的inline申请，即把定义写在class**内部**
  2. 明确声明，即在定义式前加上关键字inline

- 将大多数inlining限制在**小型、被频繁调用**的函数身上。这可使日后调试和二进制升级更容易，也可使得潜在的代码膨胀问题最小化。

- 不要只因为function templates出现在头文件，就将他们声明为inline

### 条款31：将文件间的编译依存关系降至最低

- 支持“编译依存性最小化”的思想是：相依于声明式，不要相依于定义式

- 1. 头文件和实现相分离，头文件**完全且仅有声明式**
  2. 使用创建**接口类**

### 六、继承与面向对象设计

### 条款32:确定你的public继承塑模出is-a关系

- public继承意味着**is-a**的关系，即子类是父类的一种**特殊**化，适合基类的一定适合子类，每个派生类对象含有着父类对象的特点

### 条款33：避免遮掩继承而来的名称

- 在父类中的名称会被字类的名称**覆盖**，尤其是在public继承下，没有人希望这样的发生
- 为了避免被遮掩，可以使用**using声明式**或**转交函数**，交给子类

### 条款34：区分接口继承和接口实现

- 声明**纯虚函数**的目的就是为了让派生类只继承函数接口
- 声明**虚函数**的目的是让派生类继承该函数的接口和缺省实现
- 声明**普通函数**的目的就是让派生类强制接受自己的代码，不希望重新定义

### 条款35：考虑virtual函数以外的其他选择

### 条款36：绝不重新定义继承而来的non-virtual函数

- 任何情况下都不应该**重新定义**一个继承而来的**non-virtual函数**

### 条款37：绝不重新定义继承而来的缺省参数值

- 绝对不要重新定义一个继承而来的缺省参数值，因为**缺省参数值**都是**静态绑定**的，而virtual函数——你唯一应该覆写的东西是**动态绑定**

### 条款38：通过复合塑模has-a或“根据某物实现出”

- 区分public继承和复合

- - 在应用领域，复合意味着一个中含有另一个，即has-a关系；
  - 在实现领域意味着根据某物实现出

### 条款39：明智而审慎地使用private继承

- 当需要复合时，尽可能的使用复合,必要时才使用private:

- - 当**protected成员或virtual函数**牵扯进来的时候
  - 当空间方面的利害关系，需要尺寸**最小**化

### 条款40：明智而审慎地使用多重继承

- 多重继承时候，如果其父类又继承同一个父类，所以解决的方式就是使用**virtual继承**，即其父类同时以virtual继承那个父类，但是相应的也会付出一些**代价**，例如时间更慢，需要**重新定义**父类的初始化等，因此设计时最好不要让这个父类有任何**数据成员**
- 当单一继承和多重继承都可以，那么最好选择单一继承，多重继承也有**正当**的用途，可以实现同时public继承和private继承的**组合**

### 七、模板与泛型编程

### 条款41：了解隐式接口和编译期多态

- 显式接口：由**函数的签名式**（也就是函数名称、参数类型、返回类型）构成

- 隐式接口：不基于函数签名式，而是由**有效表达式**组成

- 面向对象和泛型编程都支持接口和多态，只不过一个以**显式**为主，一个以**隐式**为主

- - 两种多态一个在**运行期**一个在**编译期**

### 条款42：了解typename的双重意义

- 声明**模板参数**的时候，class和typename是可以互换的，没什么不一样

- 但是标识**嵌套从属类型**名称的时候必须用typename

- - 不得在**基类列**（继承的时候）或**成员初值列**（初始化列表）内以它作为基类修饰符

```
templete<typename T>
class Derived:public Base<T>::Nested{ //基类列表中不可以加“typename”
public:
    explicit Derived(int x): Base<T>::Nested(x){//mem.init.list中不允许“typename”
        typename Base<T>::Nested temp; //这个是嵌套从属类型名称
        ... //作为一个基类修饰符需要加上typename
    }
}   
```

### 条款43：学习处理模板化基类内的名称

- **模板化基类**指的是当派生类的基类是一个模板

- - 但是当有模板全特化的时候，确实使用的没有这个函数，那么依然会报错
  - 当派生类中使用父类的函数时候编译器会**报错**，因为编译器不能确定当模板实例化的时候，是否真的有那个函数，因此有三种方法

- 1. 在基类函数调用之前加上  `this->`
  2. 使用 **using 声明式** ,告诉编译器，请它假设这个函数存在
  3. 指出这个函数在基类中，使用  `基类 : : 函数`  的**形式**写出来（不推荐这个，因为如果是virtual函数，则 会影响动态绑定）

### 条款44：将与参数无关的代码抽离出来

- 模板生成多个类和多个函数，所以任何模板代码都不该和某个造成膨胀的模板参数产生**相依**关系

- - 因**非类型模板参数**造成的代码膨胀，往往可以消除，做法是以**函数参数或类成员变量**替换模板参数
  - 因**类型模板参数**造成的代码膨胀，往往可以降低，做法是让带有完全相同的二进制表述 的具体类型**共享实现码**

### 条款45：运用成员函数模板接受所有兼容类型

- 使用成员函数模板可以生成接收所有兼容类型的函数
- 如果你声明成员函数模板用来泛化拷贝构造函数和赋值操作，那么你还需要声明**正常的**拷贝构造函数和赋值操作

### 条款46：需要类型转换时请为模板定义非成员函数

- 当我们编写一个模板类，它提供的和这个模板祥光的函数支持**所有参数的隐式类型转换**，请将哪些函数定义为模板类的**内部的friend函数**

### 条款47：请使用traits class表现类型信息

### 条款48：认识template元编程

### 八、定制new和delete

### 条款49：了解new—handler的行为

- 当new分配失败的时候，它会先调用一个客户指定的错误处理函数（set_new_handler），一个所谓的new—handler

- - 它是一个typedef定义出一个指针指向函数，该函数没有参数也不返回任何东西
  - set_new_handler的参数是个指针指向operator new 无法分配足够内存时该被调用的函数。其返回值也是个指针，指向set_new_handler 被调用前正在执行（马上就要被替换）的那个new—handler函数

- 一个**良好设计**的new—handler函数必须做以下事情:

- 1. **让更多内存可被使用。**此策略的一个做法是，程序一开始就分配一大块内存，而后当其第一次被调用，将它释还给程序使用
  2. **安装另一个new—handler。**可以设置让其调用另一个new—handler来替换自己，用来做不同的事情，其做法是调用set_new_handler
  3. **卸载new—handler**,也就是将null指针传给set_new_handler，这样new在分配不成功时抛出异常
  4. **抛出bad_alloc**的异常。
  5. **不返回**，调用abort或exit

- C++并部支持**类的专属**new—handler，但其实也不需要。你可以令每个类提供自己的set_new_handler和operator new即可

- set_new_handler允许客户**指定**一个函数，在内存分配**无法获得满足**时调用。

- Nothrow new是一个颇为局限的工具，因为它**只**适用于内存分配：后继的构造函数调用还是可能抛出异常

### 条款50：了解new和delete的合理替换时机

- 替换operator new或operator delete的三个常见理由：

- - 用来检测运用上的错误
  - 为了收集使用上的统计数据
  - 为了增加分配和归还的速度
  - 为了降低缺省内存管理器带来的空间额外开销，**也就是实现内存池，可以节省空间**
  - 为了弥补缺省分配器中的非最佳齐位
  - 为了将相关对象成簇集中
  - 为了获得非传统行为

- 了解何时可在“全局性的”或“class专属的”基础上合理替换缺省的new和delete

### 条款51：编写new和delete时需固守常规

- operator new应该内含有一个**无穷的循环**，并在其中尝试分配内存，如果它无法满足内存需求，就该调用new-handler。它也应该有能力处理0 bytes申请，即将其按照1 byte分配。Class 专属版本应该处理“比正确大小更大的（错误）申请”，因为**当有字类继承的时候，会出现传入的大小和父类大小不同**，所以要进行判断形如`if(size != sizeof(父类))`
- operator delete应该在收到NULL指针的时候什么也不做，必要时交给全局的operator new来处理。

### 条款52：写了placement new也要写placement delete

- 当你写一个placement operator new ,请确定也写了对应的placement operator delete版本。如果没有这样做，可能回发生**隐微而时断时续的内存泄露**
- 当你声明placement new 和placement delete,请确定不要无意识（非故意）地遮掩正常的全局版本，你如果想提供自定义形式，请**内含所有正常形式的new和delete**或**利用继承机制及using声明式**

### 九、杂项讨论

### 条款53：不要轻忽编译器的警告

- 不同的编译器有不同的警告标准，要严肃对待编译器发出的警告信息。努力在你的编译器的最高警告级别下争取“无任何警告”的荣誉
- 不要过度依赖编译器的报警能力，因为不同的编译器对待事情的态度并不相同。一旦移植到另一个编译器上，你原本依赖的警告信息有可能消失

### 条款54：让自己熟悉包括TR1在内的标准程序库

- C++标准程序库的主要机能由STL、iostream、locales组成。并包含C99标准程序库。
- TR1添加了智能指针（例如 tr1::shared_ptr）、一般化函数指针（tr1::function）、hash-based容器、正则表达式以及另外10个组件的支持
- TR1自身知识一份规范。为了获得TR1提供的好处，你需要一份实物。一个好的实物来源是Boost。

### 条款55：让自己熟悉Boost

- Boost是一个社群，也是一个网站。致力于免费、源码开放、同僚复审的C++程序库开发。Boost在C++标准化过程中扮演具有影响力的角色
- Boost提供许多TR1组件实现品，以及其他许多程序库

## 深度探索C++对象模型

**本书重点关注第一章、第五章和第六章**

#### 1.虚函数模型

分两步支持：

![image-20210908152854260](daily_update_note.assets/image-20210908152854260.png)

#### 2.成员初始化列表

下列四种情况，为了是程序能够顺利编译，必须使用成员初始化列表（member initialization list）：

![image-20210910144538967](daily_update_note.assets/image-20210910144538967.png)

#### 3.Placement Operator New （原地构造）

使用：

class AA {

};

char* addr = new char [sizeof(AA)];

AA* ptr = new(addr) AA;

在addr这个地址上构造对象



#### 4.模板相关

1.模板定义和模板具现化

![image-20210917103706468](daily_update_note.assets/image-20210917103706468.png)

![image-20210917103736389](daily_update_note.assets/image-20210917103736389.png)

![image-20210917103801530](daily_update_note.assets/image-20210917103801530.png)

在模板中，非成员的使用的名字匹配是与 “用以具现出该模板的类型参数” 有关而决定的。如果其使用互不相关，则根据模板声明时（scope of the template declaration）来决定；如果相关联，那么就以模板具现时（scope of the template instantiation）来决定。

![image-20210917104339341](daily_update_note.assets/image-20210917104339341.png)

## 模板与模板元编程

### 模板基本使用

https://mp.weixin.qq.com/s?__biz=MzI3ODQ3OTczMw==&mid=2247492957&idx=1&sn=d6fed198ace00b67d4503240e6f2da08&chksm=eb54f347dc237a51d7d5e5a67aa0ceeb8eb47e8a1f2b185f8dd7b528f0fe8f2cacfbda0596d6&mpshare=1&scene=1&srcid=1211z9ye5ESiESSSvHJVGR7d&sharer_sharetime=1639205876009&sharer_shareid=da3be6660f20e7fc03a271fc50438587&version=3.1.20.6008&platform=win#rd

### 模板元编程

https://mp.weixin.qq.com/s?__biz=MzkyODE5NjU2Mw==&mid=2247491328&idx=1&sn=a9e4147a1ffe5de9c8a7f0c8d5377405&chksm=c21d2dbcf56aa4aa5d5e60e3a4ecece847cbcb777a97f437010b1300c02ab36f9ecc01dca65d&mpshare=1&scene=1&srcid=1211fYSUN9BzKRqhpcwjdOvu&sharer_sharetime=1639205891426&sharer_shareid=da3be6660f20e7fc03a271fc50438587&version=3.1.20.6008&platform=win#rd

笔记：

参考文献：

​	深入实践C++模板编程，温宇杰著，2013（到当当网）；

​	C++程序设计语言，Bjarne Stroustrup著，裘宗燕译，2002（到当当网）；

​	C++标准，ISO/IEC 14882:2003，ISO/IEC 14882:2011（到ISO网站，C++标准委员会）；

​	wikipedia.org（C++， 模板， Template metaprogramming， Curiously recurring template pattern， Substitution failure is not an erro (SFINAE)，Expression templates， C++11， C++14）；

​	What does a call to 'this->template [somename]' do? （stackoverflow问答）；

​	Advanced C++ Lessons，chapter 6，在线教程，2005（到网站）；

​	C++ TUTORIAL - TEMPLATES - 2015，bogotobogo.com 网上教程（到网站）；

​	C++ Templates are Turing Complete，Todd L. Veldhuizen，2003（作者网站已经停了，archive.org 保存的版本，archive.org 可能被限制浏览）；

​	Metaprogramming in C++，Johannes Koskinen，2004（中科大老师保存的版本）；

​	C++ Template Metaprogramming in 15ish Minutes（Stanford 课程 PPT，到网站）；

​	Template Metaprograms，Todd Veldhuizen，1995（archive.org 保存 Todd Veldhuizen 主页，可能限制访问，在线 PS 文件转 PDF 文件网站）；

​	Expression Templates，Todd Veldhuizen，1995；

​	C++ Templates as Partial Evaluation，Todd Veldhuizen，1999；

​	Erwin Unruh 写的第一个模板元编程程序；

​	wikibooks.org（C++ Programming/Templates/Template Meta-Programming，More C++ Idioms）；

​	THE BOOST MPL LIBRARY online docs（到网站）；

​	Best introduction to C++ template metaprogramming?（stackoverflow问答）。

### 模板元语法

#### 1.关于模板特例化（详见文献[1]第4章）：

- 在定义模板特例之前必须已经有模板通例（primary template）的声明；
- 模板特例并不要求一定与通例有相同的接口，但为了方便使用（体会特例的语义）一般都相同；
- 匹配规则，在模板实例化时如果有模板通例、特例加起来多个模板版本可以匹配，则依据如下规则：对版本AB，如果 A 的模板参数取值集合是B的真子集，则优先匹配 A，如果 AB 的模板参数取值集合是“交叉”关系（AB 交集不为空，且不为包含关系），则发生编译错误，对于函数模板，用函数重载分辨（overload resolution）规则和上述规则结合并优先匹配非模板函数。



#### 2.对模板的多个实例，**类型等价**（type equivalence）判断规则（详见文献[2] 13.2.4）：

同一个模板（模板名及其参数类型列表构成的模板签名（template signature）相同，函数模板可以重载，类模板不存在重载）且指定的模板实参等价（类型参数是等价类型，非类型参数值相同）。如下例子：

```c++
#include <iostream>
// 识别两个类型是否相同，提前进入模板元编程^_^
template<typename T1, typename T2> // 通例，返回 false
class theSameType       { public: enum { ret = false }; };
template<typename T>               // 特例，两类型相同时返回 true
class theSameType<T, T> { public: enum { ret = true }; };

template<typename T, int i> class aTMP { };

int main(){
    typedef unsigned int uint; // typedef 定义类型别名而不是引入新类型
    typedef uint uint2;
    std::cout << theSameType<unsigned, uint2>::ret << '\n';
    // 感谢 C++11，连续角括号“>>”不会被当做流输入符号而编译错误
    std::cout << theSameType<aTMP<unsigned, 2>, aTMP<uint2, 2>>::ret << '\n';
    std::cout << theSameType<aTMP<int, 2>, aTMP<int, 3>>::ret << '\n';
    std::cin.get(); 
    return 0;
}
```

```
输出：
1
1
0
```

#### 3.关于**模板实例化**（template instantiation）（详见文献[4]模板）：

- 指在编译或链接时生成函数模板或类模板的具体实例源代码，即用使用模板时的实参类型替换模板类型参数（还有非类型参数和模板型参数）；
- 隐式实例化（implicit instantiation）：当使用实例化的模板时自动地在当前代码单元之前插入模板的实例化代码，模板的成员函数一直到引用时才被实例化；
- 显式实例化（explicit instantiation）：直接声明模板实例化，模板所有成员立即都被实例化；
- 实例化也是一种特例化，被称为实例化的特例（instantiated (or generated) specialization）。



隐式实例化时，成员只有被引用到才会进行实例化，这被称为推迟实例化（lazy instantiation），由此可能带来的问题如下面的例子（文献[6]，文献[7]）：

```c++
#include <iostream>

template<typename T>
class aTMP {
public:
    void f1() { std::cout << "f1()\n"; }
    void f2() { std::ccccout << "f2()\n"; } // 敲错键盘了，语义错误：没有 std::ccccout
};

int main(){
    aTMP<int> a;
    a.f1();
// a.f2(); // 这句代码被注释时，aTMP<int>::f2() 不被实例化，从而上面的错误被掩盖!
    std::cin.get(); return 0;
}
```

#### 4.关于模板的**编译和链接**（详见文献[1] 1.3、文献[4]模板）：

- 包含模板编译模式：编译器生成每个编译单元中遇到的所有的模板实例，并存放在相应的目标文件中；链接器合并等价的模板实例，生成可执行文件，要求实例化时模板定义可见，不能使用系统链接器；
- 分离模板编译模式（使用 export 关键字）：不重复生成模板实例，编译器设计要求高，可以使用系统链接器；
- 包含编译模式是主流，C++11 已经弃用 export 关键字（对模板引入 extern 新用法），一般将模板的全部实现代码放在同一个头文件中并在用到模板的地方用 #include 包含头文件，以防止出现实例不一致（如下面紧接着例子）；



实例化，编译链接的简单例子如下（参考了文献[1]第10页）：

```C

// file: a.cpp
#include <iostream>
template<typename T>
class MyClass { };
template MyClass<double>::MyClass(); // 显示实例化构造函数 MyClass<double>::MyClass()
template class MyClass<long>;        // 显示实例化整个类 MyClass<long>

template<typename T>
void print(T const& m) { std::cout << "a.cpp: " << m << '\n'; }

void fa() {
    print(1);   // print<int>，隐式实例化
    print(0.1); // print<double> 在此已实例化 double
}
void fb(); // fb() 在 b.cpp 中定义，此处声明

int main(){
    fa();
    fb();
    std::cin.get(); 
    return 0;
}
```

```c++
// file: b.cpp
#include <iostream>
template<typename T>
void print(T const& m) { std::cout << "b.cpp: " << m << '\n'; }

void fb() {
    print('2'); // print<char>
    print(0.1); // print<double>
}
```

上例中，由于 a.cpp 和 b.cpp 中的 print<double> 实例等价（模板实例的二进制代码在编译生成的对象文件 a.obj、b.obj 中），故链接时消除了一个（消除哪个没有规定，上面消除了 b.cpp 中的）。

#### 5.关于 **template**、**typename**、**this** 关键字的使用（文献[4]模板，文献[5]）：

- 依赖于模板参数（template parameter，形式参数，实参英文为 argument）的名字被称为依赖名字（dependent name），C++标准规定，如果解析器在一个模板中遇到一个嵌套依赖名字，它假定那个名字不是一个类型，除非显式用 typename 关键字前置修饰该名字；
- 和上一条 typename 用法类似，template 用于指明嵌套类型或函数为模板；
- this 用于指定查找基类中的成员（当基类是依赖模板参数的类模板实例时，由于实例化总是推迟，这时不依赖模板参数的名字不在基类中查找，文献[1]第 166 页）。



一个例子如下（需要 GCC 编译，GCC 对 C++11 几乎全面支持，VS2013 此处总是在基类中查找名字，且函数模板前不需要 template）：

```c++

#include <iostream>

template<typename T>
class aTMP{
public: typedef const T reType;
};

void f() { std::cout << "global f()\n"; }

template<typename T>
class Base {
public:
    template <int N = 99>
    void f() { std::cout << "member f(): " << N << '\n'; }
};

template<typename T>
class Derived : public Base<T> {
public:
typename T::reType m; // typename 不能省略
    Derived(typename T::reType a) : m(a) { }
    void df1() { f(); }                       // 调用全局 f()，而非想象中的基类 f()
    void df2() { this->template f(); }        // 基类 f<99>()
    void df3() { Base<T>::template f<22>(); } // 强制基类 f<22>()
    void df4() { ::f(); }                     // 强制全局 f()
};

int main(){
    Derived<aTMP<int>> a(10);
    a.df1(); a.df2(); a.df3(); a.df4();
    std::cin.get(); 
    return 0;
}
```

```

global f()
member f(): 99
member f(): 22
global f()
```

### 模板元编程概述

如果对 C++ 模板不熟悉（光熟悉语法还不算熟悉），可以先跳过本节，往下看完例子再回来。

C++ 模板最初是为实现泛型编程设计的，但人们发现模板的能力远远不止于那些设计的功能。一个重要的理论结论就是：C++ 模板是**图灵完备**的（Turing-complete），其证明过程请见文献[8]（就是用 C++ 模板模拟图灵机），理论上说 C++ 模板可以执行任何计算任务，但实际上因为模板是编译期计算，其能力受到具体编译器实现的限制（如递归嵌套深度，C++11 要求至少 1024，C++98 要求至少 17）。C++ 模板元编程是“意外”功能，而不是设计的功能，这也是 C++ 模板元编程语法丑陋的根源。

C++ 模板是图灵完备的，这使得 C++ 成为**两层次语言**（two-level languages，中文暂且这么翻译，文献[9]），其中，执行编译计算的代码称为静态代码（static code），执行运行期计算的代码称为动态代码（dynamic code），C++ 的静态代码由模板实现（预处理的宏也算是能进行部分静态计算吧，也就是能进行部分元编程，称为宏元编程，见 Boost 元编程库即 BCCL，文献[16]和文献[1] 10.4）。

具体来说 C++ 模板可以做以下事情：编译期数值计算、类型计算、代码计算（如循环展开），其中数值计算实际不太有意义，而类型计算和代码计算可以使得代码更加通用，更加易用，性能更好（也更难阅读，更难调试，有时也会有代码膨胀问题）。编译期计算在编译过程中的位置请见下图（取自文献[10]），可以看到关键是模板的机制在编译具体代码（模板实例）前执行：

![img](https://mmbiz.qpic.cn/mmbiz_png/JeibBY5FJRBGq5G0Zia9iafVntL2ZV1RWrR8wSWiavbmKqpy1xsR2O0Vesh8yASJXNCgEXib3WX5BSdib5G6XuLS7eCQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

从编程范型（programming paradigm）上来说，C++ 模板是**函数式编程**（functional programming），它的主要特点是：函数调用不产生任何副作用（没有可变的存储），用递归形式实现循环结构的功能。C++ 模板的特例化提供了条件判断能力，而模板递归嵌套提供了循环的能力，这两点使得其具有和普通语言一样通用的能力（图灵完备性）。

从**编程形式**来看，模板的“<>”中的模板参数相当于函数调用的输入参数，模板中的 typedef 或 static const 或 enum 定义函数返回值（类型或数值，数值仅支持整型，如果需要可以通过编码计算浮点数），代码计算是通过类型计算进而选择类型的函数实现的（C++ 属于静态类型语言，编译器对类型的操控能力很强）。代码示意如下：

```C++
#include <iostream>

template<typename T, int i=1>
class someComputing {
public:
  typedef volatile T* retType; // 类型计算
  enum { retValume = i + someComputing<T, i-1>::retValume }; // 数值计算，递归
  static void f() { std::cout << "someComputing: i=" << i << '\n'; }
};
template<typename T> // 模板特例，递归终止条件
class someComputing<T, 0> {
public:
  enum { retValume = 0 };
};

template<typename T>
class codeComputing {
public:
  static void f() { T::f(); } // 根据类型调用函数，代码计算
};

int main(){
    someComputing<int>::retType a=0;
    std::cout << sizeof(a) << '\n'; // 64-bit 程序指针
    // VS2013 默认最大递归深度500，GCC4.8 默认最大递归深度900（-ftemplate-depth=n）
    std::cout << someComputing<int, 500>::retValume << '\n'; // 1+2+...+500
    codeComputing<someComputing<int, 99>>::f();
    std::cin.get(); 
    return 0;
}
```

```c++
8
125250
someComputing: i=99
```

C++ 模板元编程**概览框图**如下（取自文献[9]）：

![img](https://mmbiz.qpic.cn/mmbiz_png/JeibBY5FJRBGq5G0Zia9iafVntL2ZV1RWrRykiae7bMfF1hyFw1NTGCqX8b3veUJE6XdHbnFUX0hAs2ibC4dLcrgVwg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

### 编译期数值计算

到目前为止，虽然已经看到了阶乘、求和等递归数值计算，但都没涉及原理，下面以求和为例讲解 C++ 模板编译期数值计算的原理：

```c++

#include <iostream>

template<int N>
class sumt{
public: static const int ret = sumt<N-1>::ret + N;
};
template<>
class sumt<0>{
public: static const int ret = 0;
};

int main() {
    std::cout << sumt<5>::ret << '\n';
    std::cin.get(); return 0;
}
```

```
15
```

当编译器遇到 sumt<5> 时，试图实例化之，sumt<5> 引用了 sumt<5-1> 即 sumt<4>，试图实例化 sumt<4>，以此类推，直到 sumt<0>，sumt<0> 匹配模板特例，sumt<0>::ret 为 0，sumt<1>::ret 为 sumt<0>::ret+1 为 1，以此类推，sumt<5>::ret 为 15。值得一提的是，虽然对用户来说程序只是输出了一个编译期常量 sumt<5>::ret，但在背后，编译器其实至少处理了 sumt<0> 到 sumt<5> 共 6 个类型。

从这个例子我们也可以窥探 C++ 模板元编程的函数式编程范型，对比结构化求和程序：for(i=0,sum=0; i<=N; ++i) sum+=i; 用逐步改变存储（即变量 sum）的方式来对计算过程进行编程，模板元程序没有可变的存储（都是编译期常量，是不可变的变量），要表达求和过程就要用很多个常量：sumt<0>::ret，sumt<1>::ret，...，sumt<5>::ret 。函数式编程看上去似乎效率低下（因为它和数学接近，而不是和硬件工作方式接近），但有自己的优势：描述问题更加简洁清晰（前提是熟悉这种方式），没有可变的变量就没有数据依赖，方便进行并行化。

### 模板下的控制结构





## MindSpore相关学习

### MindSpore Ascend 内存管理

https://bbs.huaweicloud.com/forum/thread-171161-1-1.html

#### SOMAS (Safe Optimized Memory Allocation Solver)

“约束编程 - 并行拓扑内存优化”(SOMAS) ：将计算图并行流与数据依赖进行聚合分析，得到算子间祖先关系构建张量全局生命期互斥约束，使用多种启发式算法求解最优的内存静态规划，实现逼近理论极限的内存复用从而提升网络训练Batch Size数量和训练吞吐量FPS。

1.FootPrint是链表，记录offset信息等

重点函数查看学习： InitSomasOutputAndWorkspaceTensors

2.github_code/mindspore/mindspore/ccsrc/backend/session/kernel_graph.h 为其图结构graph

#### 需要探索的点

1.循环内如何做生命周期维护？内存释放？

2.最优内存方案算法，多种启发式算法？

核心算法：

调用位置：mindspore/mindspore/ccsrc/backend/optimizer/somas/somas_solver_core.cc

启发式算法：github_code/mindspore/mindspore/ccsrc/backend/optimizer/somas/somas_solver_alg.cc

**bestfit算法**

```c++
// offset picking heuristics
bool SmallestFit(const pair<size_t, size_t> &a, const pair<size_t, size_t> &b) {
  return a.first < b.first || (a.first == b.first && a.second < b.second);
}
bool LargestFit(const pair<size_t, size_t> &a, const pair<size_t, size_t> &b) {
  return a.first > b.first || (a.first == b.first && a.second < b.second);
}
bool BestFit(const pair<size_t, size_t> &a, const pair<size_t, size_t> &b) {
  return a.second < b.second || (a.second == b.second && a.first < b.first);
}
bool WorstFit(const pair<size_t, size_t> &a, const pair<size_t, size_t> &b) {
  return a.second > b.second || (a.second == b.second && a.first < b.first);
}
size_t SharedObjects(FootPrint *p) { return p->Next()->getOffset(); }
size_t SingleObject(FootPrint *p) { return SIZE_MAX; }
//函数指针数组
bool (*g_pBranching[kNumFittingTypes])(const pair<size_t, size_t> &a, const pair<size_t, size_t> &b) = {
  BestFit, SmallestFit
#ifdef SOMAS_DEBUG
  ,
  LargestFit, WorstFit
#endif
};
size_t (*algorithm[kNumAlgorithmTypes])(FootPrint *p) = {SharedObjects, SingleObject};
```





### 线程池

1.单例模式，有互斥锁和条件变量，同步线程池

```c++
struct ThreadContext {

 std::mutex mutex;

 std::condition_variable cond_var;

 const Task *task{nullptr};

};//上下文结构体
```

2.在构造函数中初始化最大可用线程数

3.任务类型为using Task = std::function<int()>;

**4.使用**

```c++
            std::vector<common::Task> tasks;
            auto task = [pSolver]() {
              return pSolver->MemoryAllocationSolver() == SUCCESS ? common::SUCCESS : common::FAIL;
            };
            tasks.emplace_back(task);
			//调用
      		common::ThreadPool::GetInstance().SyncRun(tasks);


```







### 推理框架流程细节





## 服务端开发学习笔记

### 零散知识点记录

#### 1.建链

服务端和客户端连接后可分别创建连接标识进行状态维护，服务端可进行客户端的管理。

#### 2.断开

客户端主动断开，服务端通过timeing wheel检测空闲连接，并踢掉空闲连接。

#### 3.心跳协议

通过心跳消息判断对方进程是否能正常工作

#### 4.消息

简单的消息可以采用ZMQ原始的消息类型zmq::message_t；

复杂的自定义消息类型 可以通过cereal 或者protobuf进行序列化和反序列化发送。

#### 5.为什么要用回调函数

https://www.zhihu.com/question/19801131
